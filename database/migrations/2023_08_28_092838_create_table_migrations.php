<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ps_migration_categories', function (Blueprint $table) {
            $table->id();
            $table->integer('category_old_id')->nullable();
            $table->integer('category_new_id')->nullable();
        });

        Schema::create('ps_migration_products', function (Blueprint $table) {
            $table->id();
            $table->integer('product_old_id');
            $table->integer('product_new_id');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ps_migration_categories');
        Schema::dropIfExists('ps_migration_products');
    }
}
