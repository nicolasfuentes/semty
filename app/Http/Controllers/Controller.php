<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use PrestaShopWebservice;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   

    public function createTokenAccess()
    {
        // creating webservice access
        $webService = new PrestaShopWebservice(config('app.PRESTASHOP_URL'), config('app.PRESTASHOP_API_TOKEN'), false);
        return $webService;
    }
}
