<?php

namespace App\Http\Controllers;

use App\MigrationCategory;
use App\MigrationProduct;
use App\Product;
use CURLFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use PrestaShopWebserviceException;

class ProductController extends Controller
{

    protected $webService;

    public function __construct()
    {
        $this->webService = $this->createTokenAccess();
    }


    public function getProducts(Request $request)
    {
        //echo 'ProductController - getProducts<br>';
        Log::info('ProductController - getProducts');

        return $this->getList($request);
    }


    public function getList(Request $request, $saltear = 0, $tomar = 5000)
    {
        //echo 'ProductController - getList<br>';
        Log::info('ProductController - getList');

        $products = Product::skip($saltear)->take($tomar)->get();

        //echo 'count - ' . $products->count() . '<br>';
        //Log::info('count - ' . $products->count() . '<br>');

        return $products;
    }

    public function getProductPrestashop(Request $request, $product_id)
    {
        try {

            // call to retrieve  with ID 2
            $xml = $this->webService->get([
                'resource' => 'products',
                'id' => $product_id, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
            ]);

            return $xml;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo 'getProductPrestashop -- Other error: <br />';
            echo $ex->getMessage();
        }
    }

    public function getProductsPrestashop(Request $request)
    {
        try {

            // call to retrieve all 
            $xml = $this->webService->get(['resource' => 'products']);

            //dd($xml);

            $resources = $xml->products->children();

            //dd($resources);

            $productsId = [];

            foreach ($resources as $resource) {
                $attributes = $resource->attributes();
                $resourceId = $attributes['id'];
                //$resourceId = $attributes;
                // From there you could, for example, use th resource ID to call the webservice to get its details

                $productsId[] = $resourceId;
            }

            return $productsId;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo 'getProductsPrestashop -- Other error: <br />';
            echo $ex->getMessage();
        }
    }

    public function getProductSchemaPrestashop(Request $request, $print = 0)
    {
        //echo 'ProductController - getProductSchemaPrestashop <br>';
        try {

            $urlProducts = config('app.PRESTASHOP_URL') . 'api/products?schema=blank';

            // call to retrieve the blank schema
            $blankXml = $this->webService->get(['url' => $urlProducts]);

            //echo $blankXml->asXML();

            if ($print == 1) {
                dd($blankXml);
            }


            return $blankXml;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo '<br> getProductSchemaPrestashop -- Other error: <br />';
            echo $ex->getMessage();

            return false;
        }
    }

    public function updateProductPrestashop(Request $request, $product_id)
    {
        $xmlProduct = $this->getProductPrestashop($request, $product_id);


        $fields = $xmlProduct->product->children();
        $fields->firstname = 'John';
        $fields->lastname = 'DOE';

        $updatedXml = $this->webService->edit([
            'resource' => 'products',
            'id' => (int) $fields->id,
            'putXml' => $xmlProduct->asXML(),
        ]);
        
        $fields = $updatedXml->product->children();

        echo 'Product updated with ID ' . $fields->id . PHP_EOL;
    }

    public function insertProductPrestashop(Request $request)
    {
        //echo 'ProductController - insertProductPrestashopData <br>';

        $blankXml = $this->getProductSchemaPrestashop($request);

        //echo $blankXml;

        try {
            // get the entity
            $fields = $blankXml->product->children();

            //dd($fields);

            // edit entity fields

            //----------------------//
            //required

            if (isset($request['price'])) {
                $fields->price = $request['price'];
            }

            //----------------------//

            if (isset($request['id_manufacturer'])) {
                $fields->id_manufacturer = $request['id_manufacturer'];
            }

            if (isset($request['id_supplier'])) {
                $fields->id_supplier = $request['id_supplier'];
            }

            $id_category_default = 0;
            if (isset($request['id_category_default'])) {

                $migrationCategory = MigrationCategory::where('category_old_id', $request['id_category_default'])->first();
                if ($migrationCategory) {
                    $id_category_default = $migrationCategory->category_new_id;
                    $fields->id_category_default = $id_category_default;
                }
            }

            if (isset($request['new'])) {
                $fields->new = $request['new'];
            }

            if (isset($request['cache_default_attribute'])) {
                $fields->cache_default_attribute = $request['cache_default_attribute'];
            }

            //----------------------//

            if (isset($request['id_default_image'])) {
                $fields->id_default_image = $request['id_default_image'];
            }

            if (isset($request['id_default_combination'])) {
                $fields->id_default_combination = $request['id_default_combination'];
            }

            if (isset($request['id_tax_rules_group'])) {
                $fields->id_tax_rules_group = $request['id_tax_rules_group'];
            }

            if (isset($request['position_in_category'])) {
                $fields->position_in_category = $request['position_in_category'];
            }

            //----------------------//

            if (isset($request['type'])) {
                $fields->type = $request['type'];
            }

            if (isset($request['id_shop_default'])) {
                $fields->id_shop_default = $request['id_shop_default'];
            }

            if (isset($request['reference'])) {
                $fields->reference = $request['reference'];
            }

            if (isset($request['supplier_reference'])) {
                $fields->supplier_reference = $request['supplier_reference'];
            }

            //----------------------//

            if (isset($request['location'])) {
                $fields->location = $request['location'];
            }

            if (isset($request['width'])) {
                $fields->width = $request['width'];
            }

            if (isset($request['height'])) {
                $fields->height = $request['height'];
            }

            if (isset($request['depth'])) {
                $fields->depth = $request['depth'];
            }

            if (isset($request['weight'])) {
                $fields->weight = $request['weight'];
            }

            //----------------------//

            if (isset($request['quantity_discount'])) {
                $fields->quantity_discount = $request['quantity_discount'];
            }

            if (isset($request['ean13'])) {
                $fields->ean13 = $request['ean13'];
            }

            if (isset($request['isbn'])) {
                $fields->isbn = $request['isbn'];
            }

            if (isset($request['upc'])) {
                $fields->upc = $request['upc'];
            }

            if (isset($request['mpn'])) {
                $fields->mpn = $request['mpn'];
            }

            //----------------------//

            if (isset($request['cache_is_pack'])) {
                $fields->cache_is_pack = $request['cache_is_pack'];
            }

            if (isset($request['cache_has_attachments'])) {
                $fields->cache_has_attachments = $request['cache_has_attachments'];
            }

            if (isset($request['is_virtual'])) {
                $fields->is_virtual = $request['is_virtual'];
            }

            if (isset($request['state'])) {
                $fields->state = $request['state'];
            }

            if (isset($request['additional_delivery_times'])) {
                $fields->additional_delivery_times = $request['additional_delivery_times'];
            }

            //----------------------//

            if (isset($request['delivery_in_stock'])) {
                $fields->delivery_in_stock->language = $request['delivery_in_stock'];
            }

            if (isset($request['delivery_out_stock'])) {
                $fields->delivery_out_stock->language = $request['delivery_out_stock'];
            }

            if (isset($request['on_sale'])) {
                $fields->on_sale = $request['on_sale'];
            }

            if (isset($request['online_only'])) {
                $fields->online_only = $request['online_only'];
            }

            if (isset($request['ecotax'])) {
                $fields->ecotax = $request['ecotax'];
            }

            //----------------------//

            if (isset($request['minimal_quantity'])) {
                $fields->minimal_quantity = $request['minimal_quantity'];
            }

            if (isset($request['low_stock_threshold'])) {
                $fields->low_stock_threshold = $request['low_stock_threshold'];
            }

            if (isset($request['low_stock_alert'])) {
                $fields->low_stock_alert = $request['low_stock_alert'];
            }


            if (isset($request['wholesale_price'])) {
                $fields->wholesale_price = $request['wholesale_price'];
            }

            //----------------------//

            if (isset($request['unity'])) {
                $fields->unity = $request['unity'];
            }

            if (isset($request['unit_price_ratio'])) {
                $fields->unit_price_ratio = $request['unit_price_ratio'];
            }

            if (isset($request['additional_shipping_cost'])) {
                $fields->additional_shipping_cost = $request['additional_shipping_cost'];
            }


            if (isset($request['customizable'])) {
                $fields->customizable = $request['customizable'];
            }

            if (isset($request['text_fields'])) {
                $fields->text_fields = $request['text_fields'];
            }

            //----------------------//

            if (isset($request['uploadable_files'])) {
                $fields->uploadable_files = $request['uploadable_files'];
            }

            if (isset($request['active'])) {
                $fields->active = $request['active'];
            }

            if (isset($request['redirect_type'])) {
                $fields->redirect_type = $request['redirect_type'];
            }

            if (isset($request['id_type_redirected'])) {
                $fields->id_type_redirected = $request['id_type_redirected'];
            }

            if (isset($request['available_for_order'])) {
                $fields->available_for_order = $request['available_for_order'];
            }

            //----------------------//

            if (isset($request['available_date'])) {
                $fields->available_date = $request['available_date'];
            }

            if (isset($request['show_condition'])) {
                $fields->show_condition = $request['show_condition'];
            }

            if (isset($request['condition'])) {
                $fields->condition = $request['condition'];
            }

            if (isset($request['show_price'])) {
                $fields->show_price = $request['show_price'];
            }

            if (isset($request['indexed'])) {
                $fields->indexed = $request['indexed'];
            }

            //----------------------//

            if (isset($request['visibility'])) {
                $fields->visibility = $request['visibility'];
            }

            if (isset($request['advanced_stock_management'])) {
                $fields->advanced_stock_management = $request['advanced_stock_management'];
            }

            if (isset($request['date_add'])) {
                $fields->date_add = $request['date_add'];
            }

            if (isset($request['date_upd'])) {
                $fields->date_upd = $request['date_upd'];
            }

            if (isset($request['pack_stock_type'])) {
                $fields->pack_stock_type = $request['pack_stock_type'];
            }

            //----------------------//           


            if (isset($request['meta_description'])) {
                $fields->meta_description->language = $request['meta_description'];
            }
            if (isset($request['meta_keywords'])) {
                $fields->meta_keywords->language = $request['meta_keywords'];
            }

            if (isset($request['meta_title'])) {
                $fields->meta_title->language = $request['meta_title'];
            }

            if (isset($request['link_rewrite'])) {
                $fields->link_rewrite->language = $request['link_rewrite'];
            }

            if (isset($request['name'])) {
                $fields->name->language = $request['name'];
            }

            //----------------------//   

            if (isset($request['description'])) {
                $fields->description->language = $request['description'];
            }

            if (isset($request['description_short'])) {
                $fields->description_short->language = $request['description_short'];
            }

            if (isset($request['available_now'])) {
                $fields->available_now->language = $request['available_now'];
            }

            if (isset($request['available_later'])) {
                $fields->available_later->language = $request['available_later'];
            }

            /*
            if (isset($request['associations'])) {
                $fields->associations = $request['associations'];
            }
            */

            //-----------------//   
            //NO WRITABLE

            if (isset($request['manufacturer_name '])) {
                $fields->manufacturer_name = $request['manufacturer_name '];
            }

            /*
            if (isset($request['quantity'])) {
                $fields->quantity = $request['quantity'];
            }
*/
            //-----------------//   

            //var_dump($blankXml->asXML());

            // send entity to webservice
            $createdXml = $this->webService->add([
                'resource' => 'products',
                'postXml' => $blankXml->asXML(),
            ]);
            $newFields = $createdXml->product->children();
            echo '-- insertProductPrestashopData -- created with ID ' . $newFields->id . ' | category: ' . $id_category_default . PHP_EOL;

            //set id


            $newProduct = MigrationProduct::where('product_old_id', $request['id'])->first();

            if ($newProduct) {
                $newProduct->product_new_id = $newFields->id;
            } else {
                MigrationProduct::create([
                    'product_old_id' => $request['id'],
                    'product_new_id' => $newFields->id,
                ]);
            }
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo '<br> insertProductPrestashopData -- Other error: <br />';
            echo $ex->getMessage();
        }
    }


    public function insertProductPrestashopTest(Request $request)
    {
        //echo 'ProductController - insertProductPrestashopTest <br>';

        $request['id'] = 50;
        $request['price'] = 1000;


        $request['active'] = true;
        $request['name'] = 'Product1';
        $request['description'] = 'Description product1-';
        $request['description_short'] = 'ShortDescription product1-';
        $request['link_rewrite'] =  'product1';

        $request['id_category_default'] =  600;
        $request['position_in_category'] =  1;
        $request['date_add'] =  '2023-08-31';
        $request['date_upd'] =  '2023-08-31';

        $request['meta_title'] =  'Prueba Product1 -title';
        $request['meta_description'] =  'Prueba Product32-description';
        $request['meta_keywords'] =  'Prueba Product4 -keywords';

        $request['quantity'] = 1;

        return $this->insertProductPrestashop($request);
    }


    public function migrateProducts(Request $request)
    {
        //echo '<div style="font-size:10px;">';

        Log::info('ProductController - migrateProducts');
        //$this->info('migrateProducts');
        //echo 'ProductController - migrateProducts <br>';

        $saltear = config('app.SALTEAR');
        $tomar = config('app.TOMAR');
        $prestahopUrl = config('app.PRESTASHOP_URL');

        $webServices = $prestahopUrl . 'products';

        $products = $this->getList($request, $saltear, $tomar);

        $count = $products->count();

        Log::info('$count ' . $count);
        //$this->info('$count '.$count);
        //echo '$count ' . $count . '<br>';

        $process = 0;

        while ($products->count()) {
            foreach ($products as $product) {
                //echo '<br>-------------<br>';
                $requestBase = $request;

                $productLang = $product->products_lang->where('id_lang', 4)->first();

                $requestBase['id'] = $product->id_product;

                $requestBase['id_product '] = $product->id_product;
                $requestBase['id_supplier'] = $product->id_supplier; //id_supplier
                $requestBase['id_manufacturer'] = $product->id_manufacturer; //id_manufacturer
                $requestBase['id_category_default'] = $product->id_category_default; //id_category_default
                $requestBase['id_shop_default'] = $product->id_shop_default; //id_shop_default

                $requestBase['id_tax_rules_group'] = $product->id_tax_rules_group; //id_tax_rules_group
                $requestBase['on_sale'] = $product->on_sale; //on_sale
                $requestBase['online_only'] = $product->online_only; //online_only
                $requestBase['ean13'] = $product->ean13; //ean13
                $requestBase['upc'] = $product->upc; //upc

                $requestBase['ecotax'] = $product->ecotax; //ecotax
                $requestBase['quantity'] = $product->quantity; //quantity
                $requestBase['minimal_quantity'] = $product->minimal_quantity; //minimal_quantity
                $requestBase['price'] = $product->price; //price
                $requestBase['wholesale_price'] = $product->wholesale_price; //wholesale_price

                $requestBase['unity'] = $product->unity; //unity
                $requestBase['unit_price_ratio'] = $product->unit_price_ratio; //unit_price_ratio
                $requestBase['additional_shipping_cost'] = $product->additional_shipping_cost; //additional_shipping_cost
                $requestBase['reference'] = $product->reference; //reference
                $requestBase['supplier_reference'] = $product->supplier_reference; //supplier_reference

                $requestBase['location'] = $product->location; //location
                $requestBase['width'] = $product->width; //width
                $requestBase['height'] = $product->height; //height
                $requestBase['depth'] = $product->depth; //depth
                $requestBase['weight'] = $product->weight; //weight

                $requestBase['out_of_stock'] = $product->out_of_stock;
                $requestBase['quantity_discount'] = $product->quantity_discount; //quantity_discount
                $requestBase['customizable'] = $product->customizable; //customizable
                $requestBase['uploadable_files'] = $product->uploadable_files; //uploadable_files
                $requestBase['text_fields'] = $product->text_fields; //text_fields

                $requestBase['active'] = $product->active; //active
                $requestBase['redirect_type'] = $product->redirect_type; //redirect_type
                $requestBase['id_product_redirected'] = $product->id_product_redirected;
                $requestBase['available_for_order'] = $product->available_for_order; //available_for_order
                $requestBase['available_date'] = $product->available_date; //available_date

                $requestBase['condition'] = $product->condition; //condition
                $requestBase['show_price'] = $product->show_price; //show_price
                $requestBase['indexed'] = $product->indexed; //indexed
                $requestBase['visibility'] = $product->visibility; //visibility
                $requestBase['cache_is_pack'] = $product->cache_is_pack; //cache_is_pack

                $requestBase['cache_has_attachments'] = $product->cache_has_attachments; //cache_has_attachments
                $requestBase['is_virtual'] = $product->is_virtual; //is_virtual
                $requestBase['cache_default_attribute'] = $product->cache_default_attribute;    //cache_default_attribute           
                $requestBase['date_add'] =  $product->date_add; //date_add
                $requestBase['date_upd'] =  $product->date_upd; //date_upd

                $requestBase['advanced_stock_management'] = $product->advanced_stock_management; //advanced_stock_management


                if ($productLang) {
                    $requestBase['name'] = $productLang->name; //name
                    $requestBase['description'] = ($productLang->description ? $productLang->description : '--'); //description
                    $requestBase['link_rewrite'] =  $productLang->link_rewrite; //link_rewrite

                    $requestBase['meta_title'] =  $productLang->meta_title; //meta_title
                    $requestBase['meta_description'] =  $productLang->meta_description; //meta_description
                    $requestBase['meta_keywords'] =  $productLang->meta_keywords; //meta_keywords
                }

                $requestBase['state'] = 1; //state

                //$requestBase['new'] = ; //new
                //$requestBase['id_default_image'] = ; //id_default_image
                //$requestBase['id_default_combination'] = ; //id_default_combination
                //$requestBase['position_in_category'] = 1; //position_in_category
                //$requestBase['manufacturer_name'] = ; //manufacturer_name
                //$requestBase['type'] = ; //type
                //$requestBase['isbn'] = ; //isbn
                //$requestBase['mpn'] = ; //mpn

                //$requestBase['additional_delivery_times'] = ; //additional_delivery_times
                //$requestBase['delivery_in_stock'] = ; //delivery_in_stock
                //$requestBase['delivery_out_stock'] = ; //delivery_out_stock
                //$requestBase['low_stock_threshold'] = ; //low_stock_threshold
                //$requestBase['low_stock_alert'] = ; //low_stock_alert
                //$requestBase['id_type_redirected'] = ; //id_type_redirected
                //$requestBase['show_condition'] = ; //show_condition
                //$requestBase['pack_stock_type'] = ; //pack_stock_type
                //$requestBase['description_short'] = ; //description_short
                //$requestBase['available_now'] = ; //available_now
                //$requestBase['available_later'] = ; //available_later	
                //$requestBase['associations'] = ; //associations	



                //Log::info($request);

                echo '<br> <br>'   .    'id: ' . $requestBase['id'] .   ' | name: ' . $requestBase['name'];



                $this->insertProductPrestashop($requestBase);

                $process++;
            }

            $saltear = $saltear + $tomar;

            $products = $this->getList($request, $saltear, $tomar);
        }

        Log::info('$process ' . $process);
        //$this->info('$process '.$process);
        echo '<br>-----------------<br>';
        echo '$process ' . $process . '<br>';

        //echo '</div>';
        //return true;
        echo '<br> FIN';
    }


    public function deleteProducts(Request $request)
    {

        try {

            // call to retrieve all products
            $xml = $this->webService->get(['resource' => 'products']);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo '<br>deleteProducts - Other error: <br />' . $ex->getMessage();
        }

        $resources = $xml->products->children();
        foreach ($resources as $resource) {
            $attributes = $resource->attributes();
            $resourceId = $attributes['id'];
            // From there you could, for example, use th resource ID to call the webservice to get its details

            //echo '$resourceId = ' . $attributes['id'] . '<br>';

            try {

                //$id = 2;
                $this->webService->delete([
                    'resource' => 'products',
                    'id' =>  $resourceId, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
                ]);
                echo '<br><br>Product with ID ' . $resourceId . ' was successfully deleted <br>' . PHP_EOL;
            } catch (PrestaShopWebserviceException $e) {
                echo '<br>deleteProducts - Error:' . $e->getMessage();
            }
        }
        echo '<br> --------------<br>';
        echo '<br> FIN';
    }

    public function migrateProductImages(Request $request)
    {
        $saltear = 1380; //config('app.SALTEAR');
        $tomar = config('app.TOMAR');
        $products = $this->getList($request, $saltear, $tomar);

        $process = 0;
        $key  = config('app.PRESTASHOP_API_TOKEN');

        while ($products->count()) {
            foreach ($products as $product) {
                $migrationProduct = MigrationProduct::where('product_old_id', $product->id_product)->first();

                if ($migrationProduct) {
                    foreach ($product->images as $image) {
                        $idImage = $image->id_image;

                        //echo '<br> id_product: '.$product->id_product.' | id_image:'.$image->id_image;

                        $urlImage = config('app.PRESTASHOP_URL_API_IMAGES') . $migrationProduct->product_new_id . '/';

                        echo '<br>' . $urlImage;

                        $subdirImage = implode('/', str_split($idImage));
                        $file = $idImage . '-thickbox_default.jpg';

                        //Here you set the path to the image you need to upload
                        //$image_path = '/path/to/the/image.jpg';
                        $image_path = 'http://semty.mx/img/p/' . $subdirImage . '/' . $file;
                        echo '<br>' . $image_path;

                        $image_mime = 'image/jpg';

                        $args['image'] = new CURLFile($image_path, $image_mime);

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_HEADER, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
                        curl_setopt($ch, CURLOPT_URL, $urlImage);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_USERPWD, $key . ':');
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
                        $result = curl_exec($ch);
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);

                        if (200 == $httpCode) {
                            echo '<br>Product image was successfully created. - ' . $image_path;
                        }
                    }
                }
            }
            $saltear = $saltear + $tomar;

            $products = $this->getList($request, $saltear, $tomar);
        }
    }
}
