<?php

namespace App\Http\Controllers;


use App\Category;
use App\MigrationCategory;
use App\Services\CategoriesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use PrestaShopWebservice;
use PrestaShopWebserviceException;

class CategoryController extends Controller
{

    protected $webService;
    protected $categoriesAdd;
    protected $categoriesService;

    public function __construct(CategoriesService $categoriesService)
    {
        $this->webService = $this->createTokenAccess();
        $this->categoriesService = $categoriesService;
    }

    public function getCategories(Request $request)
    {
       return $this->categoriesService->getList();

    }



    public function getList(Request $request, $saltear = 0, $tomar = 5000)
    {

       return $this->categoriesService->getList();
    }


    public function getCategoryPrestashop(Request $request, $category_id)
    {
        return    $this->categoriesService->getCategoryPrestashop($category_id);

    }



    public function getCategoriesPrestashop(Request $request)
    {
        return    $this->categoriesService->getCategoriesPrestashop();
    }

    public function getCategorySchemaPrestashop(Request $request, $print = 0)
    {
        return    $this->categoriesService->getCategorySchemaPrestashop($print);

    }

    

    public function insertCategoryPrestashop(Request $request)
    {
        return    $this->categoriesService->insertCategoryPrestashop($request);
    }




   




    public function migrateCategories(Request $request)
    {
        return    $this->categoriesService->migrateCategories($request);
    }


    public function deleteCategories(Request $request)
    {
        echo '<div style="font-size:10px;">';
        echo '<br> INICIO';
        echo '<br> --------------<br>';

        return $this->categoriesService->deleteCategories();

        echo '<br> --------------<br>';
        echo '<br> FIN';
        echo '</div>';
    }


    public function getCategoryPrestashopTest(Request $request)
    {
        $category_id = 1;

        return  $this->categoriesService->getCategoryPrestashop($category_id);
    }


    public function insertCategoryPrestashopTest(Request $request)
    {
        //echo 'CategoryController - insertCategoryPrestashopTest <br>';

        $request['id'] = 50;
        $request['id_category'] = 51;
        $request['id_parent'] = 172;
        $request['is_root_category'] = false;
        $request['active'] = true;
        $request['name'] = 'PruebaCategoria2';
        $request['description'] = 'PruebaCategoria2-';
        $request['link_rewrite'] =  'PruebaCategoria2';

        $request['position'] =  20;
        $request['date_add'] =  '2023-08-31';
        $request['date_upd'] =  '2023-08-31';

        $request['meta_title'] =  'PruebaCategoria2 -title';
        $request['meta_description'] =  'PruebaCategoria 2-description';
        $request['meta_keywords'] =  'PruebaCategoria2 -keywords';

        return $this->insertCategoryPrestashop($request);
    }



    /*
    public function getCategoriesPrestashop(Request $request)
    {
        $url = env('PRESTASHOP_URL_API') . 'categories/1';
        $token = env('PRESTASHOP_API_TOKEN');

        Log::info($url);
        Log::info($token);

        $response = Curl::to($url)
            //->withHeader('Authorization: Bearer '.$token )
            //->withAuthorization($token )
            ->withData(array('ws_key' => $token))
            //->asJson()
            ->get();

        //var_dump($response);

        dd($response);

        if ($response) {

            // Read entire file into string
            //$xmlfile = file_get_contents($response);

            // Convert xml string into an object
            $new = simplexml_load_string($response);

            dd($new);

            // Convert into json
            $con = json_encode($new);

            // Convert into associative array
            $newArr = json_decode($con, true);

            //print_r($newArr);

            //return $newArr;
        } else {
            return false;
        }
    }
    */
}
