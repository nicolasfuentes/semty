<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function getImages(Request $request){        

        return $this->getList($request);
    }

    public function getList(Request $request, $saltear = 0, $tomar = 5000){
        
        $images = Image::skip($saltear)->take($tomar)->get();

        return $images;
        //return json_encode( $images);
    }
}
