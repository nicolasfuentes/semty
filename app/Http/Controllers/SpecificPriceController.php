<?php

namespace App\Http\Controllers;

use App\MigrationProduct;
use App\SpecificPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use PrestaShopWebserviceException;

class SpecificPriceController extends Controller
{
    protected $webService;

    public function __construct()
    {
        $this->webService = $this->createTokenAccess();
    }

    public function getSpecificPrices(Request $request)
    {
        return $this->getList($request);
    }



    public function getList(Request $request, $saltear = 0, $tomar = 5000)
    {

        $prices = SpecificPrice::orderBy('id_product', 'ASC')->skip($saltear)->take($tomar)->get();

        return $prices;;
    }


    public function getSpecificPricePrestashop(Request $request)
    {
        try {

            // call to retrieve  with ID 2
            $xml = $this->webService->get([
                'resource' => 'specific_prices',
                'id' => $request['id'], // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
            ]);

            return json_encode($xml);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo '<br>getSpecificPricePrestashop -- Other error: <br />';
            echo $ex->getMessage();
            return false;
        }
    }



    public function getSpecificPricesPrestashop(Request $request)
    {
        try {
            // call to retrieve all 
            $xml = $this->webService->get(['resource' => 'specific_prices']);

            //dd($xml);

            $resources = $xml->specific_prices->children();

            //dd($resources);

            $pricesId = [];

            foreach ($resources as $resource) {
                $attributes = $resource->attributes();
                $resourceId = $attributes['id'];
                // From there you could, for example, use th resource ID to call the webservice to get its details

                $pricesId[] = $resourceId;
            }

            return $pricesId;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo 'getSpecificPricesPrestashop -- Other error: <br />';
            echo $ex->getMessage();
        }
    }

    public function getSpecificPriceSchemaPrestashop(Request $request, $print = 0)
    {
        //echo 'SpecificPriceController - getSpecificPriceSchemaPrestashop <br>';

        try {

            $urlPrices = config('app.PRESTASHOP_URL') . 'api/specific_prices?schema=blank';
            //echo $urlPrices . '<br>';
            // call to retrieve the blank schema
            $blankXml = $this->webService->get(['url' => $urlPrices]);

            if($print == 1){
                dd($blankXml);
            }


            return $blankXml;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo '<br> getSpecificPriceSchemaPrestashop -- Other error: <br />';
            echo $ex->getMessage();

            return false;
        }
    }

    public function getSpecifiPricePrestashopTest(Request $request)
    {
        $request['id'] = 1;

        return $this->getSpecificPricePrestashop($request);
    }

    public function insertSpecificPricePrestashop(Request $request)
    {
       // echo 'SpecificPriceController - insertSpecificPricePrestashop <br>';

        $blankXml = $this->getSpecificPriceSchemaPrestashop($request);

        //echo $blankXml;

        try {
            // get the entity
            $fields = $blankXml->specific_price->children();

            //dd($fields);

            // edit entity fields

            //----------------------------//

            //REQUIRED

            if (isset($request['id_shop_group'])) {
                $fields->id_shop_group = $request['id_shop_group'];
            }

            if (isset($request['id_shop'])) {
                $fields->id_shop = $request['id_shop'];
            }

            if (isset($request['id_cart'])) {
                $fields->id_cart = $request['id_cart'];
            }
            //----------------------------//



            $id_product = 0;
            if (isset($request['id_product'])) {
                
                
                $migrationProduct = MigrationProduct::where('product_old_id', $request['id_product'])->first();
                if ($migrationProduct) {
                    $id_product = $migrationProduct->product_new_id;
                    
                    $fields->id_product = $id_product;
                }
            }


            if (isset($request['id_product_attribute'])) {
                $fields->id_product_attribute = $request['id_product_attribute'];
            }

            if (isset($request['id_currency'])) {
                $fields->id_currency = $request['id_currency'];
            }

            //----------------------------//

            if (isset($request['id_country'])) {
                $fields->id_country = $request['id_country'];
            }

            if (isset($request['id_group'])) {
                $fields->id_group = $request['id_group'];
            }

            if (isset($request['id_customer'])) {
                $fields->id_customer = $request['id_customer'];
            }

            //----------------------------//


            if (isset($request['id_specific_price_rule'])) {
                $fields->id_specific_price_rule = $request['id_specific_price_rule'];
            }
            if (isset($request['price'])) {
                $fields->price = $request['price'];
            }

            if (isset($request['from_quantity'])) {
                $fields->from_quantity = $request['from_quantity'];
            }

            //----------------------------//



            if (isset($request['reduction'])) {
                $fields->reduction = $request['reduction'];
            }

            $fields->reduction_tax = 0;
            if (isset($request['reduction_tax'])) {
                $fields->reduction_tax = $request['reduction_tax'];
            }

            if (isset($request['reduction_type'])) {
                $fields->reduction_type = $request['reduction_type'];
            }

            //----------------------------//

            if (isset($request['from'])) {
                $fields->from = $request['from'];
            }

            if (isset($request['to'])) {
                $fields->to = $request['to'];
            }

            //var_dump($blankXml->asXML());

            // send entity to webservice
            $createdXml = $this->webService->add([
                'resource' => 'specific_prices',
                'postXml' => $blankXml->asXML(),
            ]);
            $newFields = $createdXml->specific_price->children();
            echo '<br> insertSpecificPricePrestashop -- created with ID ' . $newFields->id . ' | $id_product: '.$id_product. PHP_EOL;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo '<br> insertSpecificPricePrestashop -- Other error: <br />';
            echo $ex->getMessage();
        }
    }



    public function migrateSpecificPrices(Request $request)
    {
        //echo '<div style="font-size:10px;">';

        Log::info('SpecificPriceController - migrateSpecificPrices');
        //echo 'SpecificPriceController - migrateSpecificPrices <br>';

        $saltear = config('app.SALTEAR');
        $tomar = config('app.TOMAR');
        $prestahopUrl = config('app.PRESTASHOP_URL');

        $webService = $prestahopUrl . 'specific_prices';

        $prices = $this->getList($request, $saltear, $tomar);

        $count = $prices->count();

        Log::info('$count ' . $count);
        //$this->info('$count '.$count);
       // echo '$count ' . $count . '<br>';

        $process = 0;

        while ($prices->count()) {
            foreach ($prices as $price) {
                //echo '<br>-------------<br>';
                $requestBase = $request;

                $requestBase['id'] = $price->id_specific_price;
                $requestBase['id_specific_price '] = $price->id_specific_price;

                $requestBase['id_specific_price_rule'] = $price->id_specific_price_rule;
                $requestBase['id_cart'] = $price->id_cart;
                $requestBase['id_product'] = $price->id_product;

                $requestBase['id_shop'] =  $price->id_shop;
                $requestBase['id_shop_group'] =  $price->id_shop_group;
                $requestBase['id_currency'] =  $price->id_currency;

                $requestBase['id_country'] =  $price->id_country;
                $requestBase['id_group'] =  $price->id_group;
                $requestBase['id_customer'] =  $price->id_customer;

                $requestBase['id_product_attribute'] =  $price->id_product_attribute;
                $requestBase['price'] =  $price->price;
                $requestBase['from_quantity'] =  $price->from_quantity;

                $requestBase['reduction'] =  $price->reduction;
                $requestBase['reduction_type'] =  $price->reduction_type;
                $requestBase['from'] =  $price->from;
                $requestBase['to'] =  $price->to;


                $requestBase['reduction_tax'] =  0;
                //Log::info($request);

                echo '<br><br> id_specific_price: ' . $requestBase['id_specific_price'] . ' | id_specific_price_rule: ' .
                 $requestBase['id_specific_price_rule']. ' | price: ' . $requestBase['price'] . ' | id_product: ' . $requestBase['id_product'] ;



                $this->insertSpecificPricePrestashop($requestBase);

                $process++;
            }

            $saltear = $saltear + $tomar;

            $prices = $this->getList($request, $saltear, $tomar);
        }

        Log::info('$process ' . $process);
        //$this->info('$process '.$process);
        echo '<br> --------------<br>';
        echo '$process ' . $process . '<br>';

        //echo '</div>';

        //return true;
        echo '<br> FIN';
    }


    public function deleteSpecificPrices(Request $request)
    {
        //echo '<div style="font-size:10px;">';

        try {

            // call to retrieve all 
            $xml = $this->webService->get(['resource' => 'specific_prices']);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo '<br>deleteSpecificPrices - Other error: <br />' . $ex->getMessage();
        }

        $resources = $xml->specific_prices->children();
        foreach ($resources as $resource) {
            $attributes = $resource->attributes();
            $resourceId = $attributes['id'];
            // From there you could, for example, use th resource ID to call the webservice to get its details

            //echo '$resourceId = ' . $attributes['id'] . '<br>';

            try {

                //$id = 2;
                $this->webService->delete([
                    'resource' => 'specific_prices',
                    'id' =>  $resourceId, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
                ]);
                echo '<br><br>Element with ID ' . $resourceId . ' was successfully deleted <br>' . PHP_EOL;
                //echo '<br>-----------------<br>';
            } catch (PrestaShopWebserviceException $e) {
                echo '<br>deleteSpecificPrices - Error:' . $e->getMessage();
            }
        }
        echo '<br> --------------<br>';
        echo '<br> FIN';
        //echo '</div>';
    }
}
