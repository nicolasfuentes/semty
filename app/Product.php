<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  /*
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_supplier` int(10) UNSIGNED DEFAULT NULL,
  `id_manufacturer` int(10) UNSIGNED DEFAULT NULL,
  `id_category_default` int(10) UNSIGNED DEFAULT NULL,
  `id_shop_default` int(10) UNSIGNED NOT NULL DEFAULT 1,

  `id_tax_rules_group` int(11) UNSIGNED NOT NULL,
  `on_sale` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `online_only` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,

  `ecotax` decimal(17,6) NOT NULL DEFAULT 0.000000,
  `quantity` int(10) NOT NULL DEFAULT 0,
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `price` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT 0.000000,

  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `additional_shipping_cost` decimal(20,2) NOT NULL DEFAULT 0.00,
  `reference` varchar(32) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,

  `location` varchar(64) DEFAULT NULL,
  `width` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `height` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `depth` decimal(20,6) NOT NULL DEFAULT 0.000000,
  `weight` decimal(20,6) NOT NULL DEFAULT 0.000000,

  `out_of_stock` int(10) UNSIGNED NOT NULL DEFAULT 2,
  `quantity_discount` tinyint(1) DEFAULT 0,
  `customizable` tinyint(2) NOT NULL DEFAULT 0,
  `uploadable_files` tinyint(4) NOT NULL DEFAULT 0,
  `text_fields` tinyint(4) NOT NULL DEFAULT 0,

  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `redirect_type` enum('','404','301','302') NOT NULL DEFAULT '',
  `id_product_redirected` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `available_for_order` tinyint(1) NOT NULL DEFAULT 1,
  `available_date` date NOT NULL,

  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT 1,
  `indexed` tinyint(1) NOT NULL DEFAULT 0,
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_is_pack` tinyint(1) NOT NULL DEFAULT 0,

  `cache_has_attachments` tinyint(1) NOT NULL DEFAULT 0,
  `is_virtual` tinyint(1) NOT NULL DEFAULT 0,
  `cache_default_attribute` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,

  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT 0
  */

  protected $table = 'ps_product';

  protected $fillable = [
    'id_product',
    'id_supplier',
    'id_manufacturer',
    'id_category_default',
    'id_shop_default',
    'id_tax_rules_group',
    'on_sale',
    'online_only',
    'ean13',
    'upc',
    'ecotax',
    'quantity',
    'minimal_quantity',
    'price',
    'wholesale_price',
    'unity',
    'unit_price_ratio',
    'additional_shipping_cost',
    'reference',
    'supplier_reference',
    'location',
    'width',
    'height',
    'depth',
    'weight',
    'out_of_stock',
    'quantity_discount',
    'customizable',
    'uploadable_files',
    'text_fields',
    'active',
    'redirect_type',
    'id_product_redirected',
    'available_for_order',
    'available_date',
    'condition',
    'show_price',
    'indexed',
    'visibility',
    'cache_is_pack',
    'cache_has_attachments',
    'is_virtual',
    'cache_default_attribute',
    'date_add',
    'date_upd',
    'advanced_stock_management'
  ];

  // RELATIONS
  public function products_lang()
  {
    return $this->hasMany('App\ProductLang', 'id_product', 'id_product');
  }

  public function images()
  {
    return $this->hasMany('App\Image', 'id_product', 'id_product');
  }
}
