<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /*
    `id_image` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `position` smallint(2) UNSIGNED NOT NULL DEFAULT 0,
  `cover` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
  */

  protected $table = 'ps_image';

  protected $fillable = [
    'id_image',
    'id_product',
    'position',
    'cover'
  ];
}
