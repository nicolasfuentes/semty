<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MigrationCategory extends Model
{
    protected $table = 'ps_migration_categories';

    protected $fillable = [
        'category_old_id',
        'category_new_id'
    ];

    public $timestamps = false;
}
