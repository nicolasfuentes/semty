<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecificPrice extends Model
{
    /*
 `id_specific_price` int(10) UNSIGNED NOT NULL,
  `id_specific_price_rule` int(11) UNSIGNED NOT NULL,
  `id_cart` int(11) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
  `id_shop_group` int(11) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `price` decimal(20,6) NOT NULL,
  `from_quantity` mediumint(8) UNSIGNED NOT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL
    */

    protected $table = 'ps_specific_price';


    protected $fillable = [
        'id_specific_price' ,
        'id_specific_price_rule' ,
        'id_cart' ,
        'id_product',
        'id_shop' ,
        'id_shop_group' ,
        'id_currency' ,
        'id_country' ,
        'id_group' ,
        'id_customer' ,
        'id_product_attribute' ,
        'price' ,
        'from_quantity' ,
        'reduction',
        'reduction_type' ,
        'from'  ,
        'to'  
    ];
}
