<?php

namespace App\Services;

use App\Category;
use App\MigrationCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PrestaShopWebserviceException;

class CategoriesService
{

    protected $prestashopServices;
    protected $webService;
    protected $blankXml;

    public function __construct(
        PrestashopService $prestashopServices
    ) {
        $this->prestashopServices = $prestashopServices;
        $this->webService = $prestashopServices->createTokenAccess();
        $this->blankXml = $this->getCategorySchemaPrestashop();
    }



    public function deleteCategories()
    {
        Log::info('==========================================');
        Log::info('deleteCategories - start');
        $start = Carbon::now();

        try {

            // call to retrieve all 
            $xml = $this->webService->get(['resource' => 'categories']);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            //echo '<br>deleteCategories - Other error: <br />' . $ex->getMessage();
            Log::info('deleteCategories - Other error:' . $ex->getMessage());
        }

        $resources = $xml->categories->children();
        foreach ($resources as $resource) {
            $attributes = $resource->attributes();
            $resourceId = $attributes['id'];
            // From there you could, for example, use th resource ID to call the webservice to get its details

            // echo '$resourceId = ' . $attributes['id'] . '<br>';

            if ($resourceId > 2) {
                try {

                    //$id = 2;
                    $this->webService->delete([
                        'resource' => 'categories',
                        'id' =>  $resourceId, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
                    ]);
                    //echo '<br><br>Element with ID ' . $resourceId . ' was successfully deleted <br>' . PHP_EOL;
                    //echo '<br>-----------------<br>';
                    Log::info('Element with ID ' . $resourceId . ' was successfully deleted');
                } catch (PrestaShopWebserviceException $ex) {
                    //echo '<br>deleteCategories - Error:' . $e->getMessage();
                    Log::info('deleteCategories - resource - Other error:' . $ex->getMessage());
                }
            }
        }

        $end = Carbon::now();
        Log::info('deleteCategories - end - ' . $start->diffInSeconds($end) . ' seconds');
        Log::info('==========================================');
    }


    public function migrateCategories()
    {
        $request = [];
        Log::info('==========================================');
        Log::info('migrateCategories - start');
        $start = Carbon::now();

        $saltear = 2; //config('app.SALTEAR');
        $tomar = config('app.TOMAR');
        $prestahopUrl = config('app.PRESTASHOP_URL');

        $webServiceCategories = $prestahopUrl . 'categories';

        $categories = $this->getList($saltear, $tomar);

        $count = $categories->count();

        Log::info('$count ' . $count);

        $process = 0;

        $categoriesStore = $this->getCategoriesPrestashop();

        while ($categories->count()) {
            foreach ($categories as $category) {

                $categoryLang = $category->categories_lang->where('id_lang', 7)->first();


                $migrationCategory = MigrationCategory::where('category_old_id',  $category->id_category)->first();

                if (!$migrationCategory  || !array_search($migrationCategory->category_new_id, $categoriesStore)) {

                    $requestBase = [];


                    $requestBase['id'] = $category->id_category;
                    $requestBase['id_category '] = $category->id_category;
                    $requestBase['id_parent'] = $category->id_parent;
                    $requestBase['is_root_category'] = $category->is_root_category;
                    $requestBase['active'] = $category->active;

                    $requestBase['name'] = $categoryLang->name;
                    $requestBase['description'] = $categoryLang->description;
                    $requestBase['link_rewrite'] =  $categoryLang->link_rewrite;

                    $requestBase['position'] =  $category->position;
                    $requestBase['date_add'] =  $category->date_add;
                    $requestBase['date_upd'] =  $category->date_upd;

                    $requestBase['meta_title'] =  $categoryLang->meta_title;
                    $requestBase['meta_description'] =  $categoryLang->meta_description;
                    $requestBase['meta_keywords'] =  $categoryLang->meta_keywords;

                    //Log::info($request);

                    Log::info('id: ' . $requestBase['id'] .  ' | name: ' . $requestBase['name'] .  ' | id_parent: ' . $requestBase['id_parent']);

                    $this->insertCategoryPrestashop($requestBase);

                    $process++;
                } else {
                    Log::info('category exist: ' . $category->id_category . ' | ' . $categoryLang->name);
                }
            }

            $saltear = $saltear + $tomar;

            $categories = $this->getList($saltear, $tomar);
        }



        $end = Carbon::now();
        Log::info('migrateCategories - ' . $process . ' - end - ' . $start->diffInSeconds($end) . ' seconds');
        Log::info('==========================================');
    }


    public function getList($saltear = 0, $tomar = 5000)
    {

        $categories = Category::skip($saltear)->take($tomar)->get();


        return $categories;
    }


    public function insertCategoryPrestashop($request)
    {
        $blankXml = $this->blankXml;




        try {
            // get the entity
            $fields = $blankXml->category->children();

            // edit entity fields
            //----------------------------//

            //REQUIRED

            if (isset($request['active'])) {
                $fields->active = $request['active'];
            }

            if (isset($request['name'])) {
                $fields->name->language = $request['name'];
            }

            if (isset($request['link_rewrite'])) {
                $fields->link_rewrite->language = $request['link_rewrite'];
            }
            //----------------------------//

            /*
            if (isset($request['id_category'])) {
                $fields->id = $request['id_category'];
            }
            */

            $id_parent = 0;
            if (isset($request['id_parent'])) {
                if ($request['id_parent'] > 2) {
                    $migrationCategory = MigrationCategory::where('category_old_id', $request['id_parent'])->first();
                    if ($migrationCategory) {
                        $id_parent = $migrationCategory->category_new_id;
                        $fields->id_parent = $id_parent;
                    }
                } else {
                    $id_parent = $request['id_parent'];
                    $fields->id_parent = $id_parent;
                }
            }


            if (isset($request['is_root_category'])) {
                $fields->is_root_category = $request['is_root_category'];
            }


            if (isset($request['id_shop_default'])) {
                $fields->id_shop_default = $request['id_shop_default'];
            }


            if (isset($request['position'])) {
                $fields->position = $request['position'];
            }
            if (isset($request['date_add'])) {
                $fields->date_add = $request['date_add'];
            }
            if (isset($request['date_upd'])) {
                $fields->date_upd = $request['date_upd'];
            }

            if (isset($request['description'])) {
                $fields->description->language = $request['description'];
            }

            if (isset($request['meta_title'])) {
                $fields->meta_title->language = $request['meta_title'];
            }
            if (isset($request['meta_description'])) {
                $fields->meta_description->language = $request['meta_description'];
            }
            if (isset($request['meta_keywords'])) {
                $fields->meta_keywords->language = $request['meta_keywords'];
            }


            // send entity to webservice
            $createdXml = $this->webService->add([
                'resource' => 'categories',
                'postXml' => $blankXml->asXML(),
            ]);
            $newFields = $createdXml->category->children();

            Log::info('-- insertCategoryPrestashopData -- created with ID ' . $newFields->id . ' | $id_parent: ' . $id_parent);

            //set id


            $newCategory = MigrationCategory::where('category_old_id', $request['id'])->first();

            if ($newCategory) {
                $newCategory->category_new_id = $newFields->id;
                $newCategory->save();
            } else {
                MigrationCategory::create([
                    'category_old_id' => $request['id'],
                    'category_new_id' => $newFields->id,
                ]);
            }
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            //echo '<br> insertCategoryPrestashopData -- Other error: <br />';
            //echo $ex->getMessage();

            Log::info('insertCategoryPrestashopData -- Other error: ' . $ex->getMessage());
        }
    }


    public function getCategorySchemaPrestashop($print = 0)
    {
        Log::info('------------------------------------------');
        try {

            $urlCategories = config('app.PRESTASHOP_URL') . 'api/categories?schema=blank';
            Log::info($urlCategories);
            //echo $urlCategories.'<br>';
            // call to retrieve the blank schema
            $blankXml = $this->webService->get(['url' => $urlCategories]);

            if ($print == 1) {
                dd($blankXml);
            }

            Log::info($blankXml->asXML());

            return $blankXml;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            // echo '<br> getCategorySchemaPrestashop -- Other error: <br />';
            // echo $ex->getMessage();

            Log::info('getCategorySchemaPrestashop -- Other error:' . $ex->getMessage());

            return false;
        }

        Log::info('------------------------------------------');
    }


    public function getCategoriesPrestashop()
    {
        try {

            // call to retrieve all 
            $xml = $this->webService->get(['resource' => 'categories']);

            //dd($xml);

            $resources = $xml->categories->children();

            //dd($resources);

            $categoriesId = [];

            foreach ($resources as $resource) {
                $attributes = $resource->attributes();
                $resourceId = $attributes['id'];
                // From there you could, for example, use th resource ID to call the webservice to get its details

                $categoriesId[] = $resourceId;
            }

            //Log::info('$categoriesId');
            //Log::info($categoriesId);

            /*
            $categoriesArray = [];

            foreach ($categoriesId as $categoryId) {
                $category = $this->getCategoryPrestashop($categoryId);
                if($category )
                $categoriesArray[$categoryId] =  $category->name;
            }

            return $categoriesArray;
            */

            return $categoriesId;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('getCategoriesPrestashop -- Other error: ' . $ex->getMessage());
        }
    }

    public function getCategoryPrestashop($category_id)
    {
        try {

            // call to retrieve customer with ID 2
            $xml = $this->webService->get([
                'resource' => 'categories',
                'id' => $category_id, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
            ]);

            $category = $xml->category->children();

            return $category;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('getCategoryPrestashop -- Other error: ' . $ex->getMessage());
            return false;
        }
    }
}
