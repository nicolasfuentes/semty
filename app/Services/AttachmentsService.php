<?php

namespace App\Services;

use App\Attachment;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PrestaShopWebserviceException;

class AttachmentsService
{
    protected $prestashopServices;
    protected $webService;
    protected $blankXml;

    public function __construct(
        PrestashopService $prestashopServices
    ) {
        $this->prestashopServices = $prestashopServices;
        $this->webService = $prestashopServices->createTokenAccess();
        $this->blankXml = $this->getAttachmentSchemaPrestashop();
    }

    public function getAttachments(Request $request)
    {
        return $this->getList();
    }



    public function getList($saltear = 0, $tomar = 5000)
    {

        $list = Attachment::orderBy('id_attachment', 'ASC')->skip($saltear)->take($tomar)->get();

        return $list;
    }


    public function getAttachmentPrestashop(Request $request, $id)
    {
        try {

            // call to retrieve  with ID 2
            $xml = $this->webService->get([
                'resource' => 'attachments',
                'id' => $id, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
            ]);

            return json_encode($xml);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('getAttachmentPrestashop -- Other error: ' .             $ex->getMessage());
            return false;
        }
    }



    public function getAttachmentsPrestashop(Request $request)
    {
        try {
            // call to retrieve all 
            $xml = $this->webService->get(['resource' => 'attachments']);

            //dd($xml);

            $resources = $xml->attachments->children();

            //dd($resources);

            $pricesId = [];

            foreach ($resources as $resource) {
                $attributes = $resource->attributes();
                $resourceId = $attributes['id'];
                // From there you could, for example, use th resource ID to call the webservice to get its details

                $pricesId[] = $resourceId;
            }

            return $pricesId;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('getAttachmentsPrestashop -- Other error: ' .             $ex->getMessage());
        }
    }

    public function getAttachmentSchemaPrestashop($print = 0)
    {

        try {

            $url = config('app.PRESTASHOP_URL') . 'api/attachments?schema=blank';
            // call to retrieve the blank schema
            $blankXml = $this->webService->get(['url' => $url]);

            if ($print == 1) {
                dd($blankXml);
            }

            Log::info($blankXml->asXML());

            return $blankXml;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info(' getAttachmentSchemaPrestashop -- Other error: ' . $ex->getMessage());

            return false;
        }
    }



    public function insertAttachmentPrestashop($request)
    {

        $blankXml = $this->getAttachmentSchemaPrestashop($request);


        try {
            // get the entity
            $fields = $blankXml->attachment->children();

            //dd($fields);

            // edit entity fields

            //----------------------------//

            //REQUIRED

            $id_attachment = 0;
            if (isset($request['id_attachment'])) {
                $id_attachment = $request['id_attachment'];
                $fields->id_attachment = $request['id_attachment'];
            }

            if (isset($request['file'])) {
                $fields->file = $request['file'];
            }

            if (isset($request['file_name'])) {
                $fields->file_name = $request['file_name'];
            }

            if (isset($request['mime'])) {
                $fields->mime = $request['mime'];
            }
            //----------------------------//

            if (isset($request['name'])) {
                $fields->name = $request['name'];
            }

            if (isset($request['description'])) {
                $fields->description = $request['description'];
            }            



            //var_dump($blankXml->asXML());

            Log::info($blankXml->asXML());
            
            // send entity to webservice
            $createdXml = $this->webService->add([
                'resource' => 'attachments',
                'postXml' => $blankXml->asXML(),
            ]);
            $newFields = $createdXml->attachment->children();
            Log::info('insertAttachmentPrestashop -- created with ID ' . $newFields->id . ' | $id_attachment: ' . $id_attachment);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('insertAttachmentPrestashop -- Other error: ' . $ex->getMessage());
        }
    }



    public function migrateAttachments()
    {

        Log::info('=================================================');
        Log::info('migrateAttachments - start');
        $start = Carbon::now();


        $saltear = config('app.SALTEAR');
        $tomar = config('app.TOMAR');
        $prestahopUrl = config('app.PRESTASHOP_URL');

        $webService = $prestahopUrl . 'attachments';

        $prices = $this->getList($saltear, $tomar);

        $count = $prices->count();

        Log::info('$count ' . $count);

        $process = 0;

        $request = [];

        while ($prices->count()) {
            foreach ($prices as $price) {

                $attachmentLang = null;

                if ($price->attachments_lang) {
                    $attachmentLang = $price->attachments_lang->where('id_lang', 7)->first();
                }

                $requestBase = $request;

                $requestBase['id_attachment '] = $price->id_attachment;

                $requestBase['file'] = $price->file;
                $requestBase['file_name'] = $price->file_name;
                $requestBase['mime'] = $price->mime;

                $requestBase['name'] = '---';
                $requestBase['description'] = '---';

                if ($attachmentLang) {
                    $requestBase['name'] = $attachmentLang->name;
                    $requestBase['description'] = $attachmentLang->description;
                }

                Log::info($requestBase);

                //Log::info($requestBase['id_attachment'] . ' | ' . $requestBase['file_name'] . ' | ' . $requestBase['name']);




                $this->insertAttachmentPrestashop($requestBase);

                $process++;
            }

            $saltear = $saltear + $tomar;

            $prices = $this->getList($saltear, $tomar);
        }

        Log::info('$process ' . $process);

        $end = Carbon::now();
        Log::info('migrateAttachments - end - ' . $start->diffInSeconds($end) . ' seconds');
        Log::info('=================================================');
    }


    public function deleteAttachments()
    {
        Log::info('=================================================');
        Log::info('deleteAttachments - start');
        $start = Carbon::now();

        try {

            // call to retrieve all 
            $xml = $this->webService->get(['resource' => 'attachments']);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('deleteAttachments - Other error: ' . $ex->getMessage());
        }

        $resources = $xml->attachments->children();
        foreach ($resources as $resource) {
            $attributes = $resource->attributes();
            $resourceId = $attributes['id'];
            // From there you could, for example, use th resource ID to call the webservice to get its details


            try {

                //$id = 2;
                $this->webService->delete([
                    'resource' => 'attachments',
                    'id' =>  $resourceId, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
                ]);
                Log::info('Element with ID ' . $resourceId . ' was successfully deleted ');
            } catch (PrestaShopWebserviceException $e) {
                Log::info('deleteAttachments - Error:' . $e->getMessage());
            }
        }
        $end = Carbon::now();
        Log::info('deleteAttachments - end - ' . $start->diffInSeconds($end) . ' seconds');
        Log::info('=================================================');
    }
}
