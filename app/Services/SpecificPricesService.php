<?php

namespace App\Services;

use App\MigrationCategory;
use App\Product;
use App\MigrationProduct;
use App\SpecificPrice;
use Carbon\Carbon;
use CURLFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PrestaShopWebserviceException;

class SpecificPricesService
{
    protected $prestashopServices;
    protected $webService;
    protected $blankXml;

    public function __construct(
        PrestashopService $prestashopServices
    ) {
        $this->prestashopServices = $prestashopServices;
        $this->webService = $prestashopServices->createTokenAccess();
        $this->blankXml = $this->getSpecificPriceSchemaPrestashop();
    }

    public function getSpecificPrices(Request $request)
    {
        return $this->getList();
    }



    public function getList($saltear = 0, $tomar = 5000)
    {

        $prices = SpecificPrice::orderBy('id_product', 'ASC')->skip($saltear)->take($tomar)->get();

        return $prices;
    }


    public function getSpecificPricePrestashop(Request $request, $id)
    {
        try {

            // call to retrieve  with ID 2
            $xml = $this->webService->get([
                'resource' => 'specific_prices',
                'id' => $id, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
            ]);

            return json_encode($xml);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('getSpecificPricePrestashop -- Other error: ' .             $ex->getMessage());
            return false;
        }
    }



    public function getSpecificPricesPrestashop(Request $request)
    {
        try {
            // call to retrieve all 
            $xml = $this->webService->get(['resource' => 'specific_prices']);

            //dd($xml);

            $resources = $xml->specific_prices->children();

            //dd($resources);

            $pricesId = [];

            foreach ($resources as $resource) {
                $attributes = $resource->attributes();
                $resourceId = $attributes['id'];
                // From there you could, for example, use th resource ID to call the webservice to get its details

                $pricesId[] = $resourceId;
            }

            return $pricesId;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('getSpecificPricesPrestashop -- Other error: ' .             $ex->getMessage());
        }
    }

    public function getSpecificPriceSchemaPrestashop($print = 0)
    {

        try {

            $urlPrices = config('app.PRESTASHOP_URL') . 'api/specific_prices?schema=blank';
            // call to retrieve the blank schema
            $blankXml = $this->webService->get(['url' => $urlPrices]);

            if ($print == 1) {
                dd($blankXml);
            }


            return $blankXml;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info(' getSpecificPriceSchemaPrestashop -- Other error: ' . $ex->getMessage());

            return false;
        }
    }



    public function insertSpecificPricePrestashop($request)
    {

        $blankXml = $this->getSpecificPriceSchemaPrestashop($request);


        try {
            // get the entity
            $fields = $blankXml->specific_price->children();

            //dd($fields);

            // edit entity fields

            //----------------------------//

            //REQUIRED

            if (isset($request['id_shop_group'])) {
                $fields->id_shop_group = $request['id_shop_group'];
            }

            if (isset($request['id_shop'])) {
                $fields->id_shop = $request['id_shop'];
            }

            if (isset($request['id_cart'])) {
                $fields->id_cart = $request['id_cart'];
            }
            //----------------------------//



            $id_product = 0;
            if (isset($request['id_product'])) {


                $migrationProduct = MigrationProduct::where('product_old_id', $request['id_product'])->first();
                if ($migrationProduct) {
                    $id_product = $migrationProduct->product_new_id;

                    $fields->id_product = $id_product;
                }
            }


            if (isset($request['id_product_attribute'])) {
                $fields->id_product_attribute = $request['id_product_attribute'];
            }

            if (isset($request['id_currency'])) {
                $fields->id_currency = $request['id_currency'];
            }

            //----------------------------//

            if (isset($request['id_country'])) {
                $fields->id_country = $request['id_country'];
            }

            if (isset($request['id_group'])) {
                $fields->id_group = $request['id_group'];
            }

            if (isset($request['id_customer'])) {
                $fields->id_customer = $request['id_customer'];
            }

            //----------------------------//


            if (isset($request['id_specific_price_rule'])) {
                $fields->id_specific_price_rule = $request['id_specific_price_rule'];
            }
            if (isset($request['price'])) {
                $fields->price = $request['price'];
            }

            if (isset($request['from_quantity'])) {
                $fields->from_quantity = $request['from_quantity'];
            }

            //----------------------------//



            if (isset($request['reduction'])) {
                $fields->reduction = $request['reduction'];
            }

            $fields->reduction_tax = 0;
            if (isset($request['reduction_tax'])) {
                $fields->reduction_tax = $request['reduction_tax'];
            }

            if (isset($request['reduction_type'])) {
                $fields->reduction_type = $request['reduction_type'];
            }

            //----------------------------//

            if (isset($request['from'])) {
                $fields->from = $request['from'];
            }

            if (isset($request['to'])) {
                $fields->to = $request['to'];
            }

            //var_dump($blankXml->asXML());

            // send entity to webservice
            $createdXml = $this->webService->add([
                'resource' => 'specific_prices',
                'postXml' => $blankXml->asXML(),
            ]);
            $newFields = $createdXml->specific_price->children();
            Log::info('insertSpecificPricePrestashop -- created with ID ' . $newFields->id . ' | $id_product: ' . $id_product);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('insertSpecificPricePrestashop -- Other error: ' . $ex->getMessage());
        }
    }



    public function migrateSpecificPrices()
    {

        Log::info('=================================================');
        Log::info('migrateSpecificPrices - start');
        $start = Carbon::now();

        Log::info('SpecificPriceController - migrateSpecificPrices');

        $saltear = config('app.SALTEAR');
        $tomar = config('app.TOMAR');
        $prestahopUrl = config('app.PRESTASHOP_URL');

        $webService = $prestahopUrl . 'specific_prices';

        $prices = $this->getList($saltear, $tomar);

        $count = $prices->count();

        Log::info('$count ' . $count);

        $process = 0;

        $request = [];

        while ($prices->count()) {
            foreach ($prices as $price) {
                $requestBase = $request;

                $requestBase['id'] = $price->id_specific_price;
                $requestBase['id_specific_price '] = $price->id_specific_price;

                $requestBase['id_specific_price_rule'] = $price->id_specific_price_rule;
                $requestBase['id_cart'] = $price->id_cart;
                $requestBase['id_product'] = $price->id_product;

                $requestBase['id_shop'] =  $price->id_shop;
                $requestBase['id_shop_group'] =  $price->id_shop_group;
                $requestBase['id_currency'] =  $price->id_currency;

                $requestBase['id_country'] =  $price->id_country;
                $requestBase['id_group'] =  $price->id_group;
                $requestBase['id_customer'] =  $price->id_customer;

                $requestBase['id_product_attribute'] =  $price->id_product_attribute;
                $requestBase['price'] =  $price->price;
                $requestBase['from_quantity'] =  $price->from_quantity;

                $requestBase['reduction'] =  $price->reduction;
                $requestBase['reduction_type'] =  $price->reduction_type;
                $requestBase['from'] =  $price->from;
                $requestBase['to'] =  $price->to;


                $requestBase['reduction_tax'] =  1;
                Log::info($requestBase);

                Log::info($requestBase['id'] . ' | ' . $requestBase['id_product'] . ' | ' . $requestBase['price']);

                //Log::info( ' id_specific_price: ' . $requestBase['id_specific_price'] . ' | id_specific_price_rule: ' .      $requestBase['id_specific_price_rule']. ' | price: ' . $requestBase['price'] . ' | id_product: ' . $requestBase['id_product']) ;



                $this->insertSpecificPricePrestashop($requestBase);

                $process++;
            }

            $saltear = $saltear + $tomar;

            $prices = $this->getList($saltear, $tomar);
        }

        Log::info('$process ' . $process);

        $end = Carbon::now();
        Log::info('migrateSpecificPrices - end - ' . $start->diffInSeconds($end) . ' seconds');
        Log::info('=================================================');
    }


    public function deleteSpecificPrices()
    {
        Log::info('=================================================');
        Log::info('deleteSpecificPrices - start');
        $start = Carbon::now();

        try {

            // call to retrieve all 
            $xml = $this->webService->get(['resource' => 'specific_prices']);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('deleteSpecificPrices - Other error: ' . $ex->getMessage());
        }

        $resources = $xml->specific_prices->children();
        foreach ($resources as $resource) {
            $attributes = $resource->attributes();
            $resourceId = $attributes['id'];
            // From there you could, for example, use th resource ID to call the webservice to get its details


            try {

                //$id = 2;
                $this->webService->delete([
                    'resource' => 'specific_prices',
                    'id' =>  $resourceId, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
                ]);
                Log::info('Element with ID ' . $resourceId . ' was successfully deleted ');
            } catch (PrestaShopWebserviceException $e) {
                Log::info('deleteSpecificPrices - Error:' . $e->getMessage());
            }
        }
        $end = Carbon::now();
        Log::info('deleteSpecificPrices - end - ' . $start->diffInSeconds($end) . ' seconds');
        Log::info('=================================================');
    }
}
