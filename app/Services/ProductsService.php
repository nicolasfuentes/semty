<?php

namespace App\Services;

use App\MigrationCategory;
use App\Product;
use App\MigrationProduct;
use Carbon\Carbon;
use CURLFile;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PrestaShopWebserviceException;

class ProductsService
{
    protected $prestashopServices;
    protected $webService;
    protected $blankXml;

    public function __construct(
        PrestashopService $prestashopServices
    ) {
        $this->prestashopServices = $prestashopServices;
        $this->webService = $prestashopServices->createTokenAccess();
        $this->blankXml = $this->getProductSchemaPrestashop();
    }

    public function getProducts()
    {
        Log::info('ProductController - getProducts');

        return $this->getList();
    }


    public function getList($saltear = 0, $tomar = 5000)
    {
        Log::info('ProductController - getList - $saltear ' . $saltear . ' - $tomar ' . $tomar);

        $products = Product::skip($saltear)->take($tomar)->get();



        return $products;
    }

    public function getProductPrestashop($product_id)
    {
        try {

            // call to retrieve  with ID 2
            $xml = $this->webService->get([
                'resource' => 'products',
                'id' => $product_id, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
            ]);

            return $xml;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('getProductPrestashop -- Other error: ' .    $ex->getMessage());
            return false;
        }
    }

    public function getProductsPrestashop()
    {
        try {

            // call to retrieve all 
            $xml = $this->webService->get(['resource' => 'products']);

            //dd($xml);

            $resources = $xml->products->children();

            //dd($resources);

            $productsId = [];

            foreach ($resources as $resource) {
                $attributes = $resource->attributes();
                $resourceId = $attributes['id'];
                //$resourceId = $attributes;
                // From there you could, for example, use th resource ID to call the webservice to get its details

                $productsId[] = $resourceId;
            }

            return $productsId;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('getProductsPrestashop -- Other error: ' . $ex->getMessage());
        }
    }

    public function getProductSchemaPrestashop($print = 0)
    {
        try {

            $urlProducts = config('app.PRESTASHOP_URL') . 'api/products?schema=blank';

            // call to retrieve the blank schema
            $blankXml = $this->webService->get(['url' => $urlProducts]);


            if ($print == 1) {
                dd($blankXml);
            }


            return $blankXml;
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info(' getProductSchemaPrestashop -- Other error: ' . $ex->getMessage());

            return false;
        }
    }


    public function updateProductPrestashop($product_id, $data)
    {
        //Log::info('updateProductPrestashop');

        $xmlProduct = $this->getProductPrestashop($product_id);
        //Log::info($xmlProduct->asXML());

        if ($xmlProduct) {
            $fields = $xmlProduct->product->children();
            //Log::info($fields);

            if (isset($data['name'])) {
                $fields->name = $data['name'];
            }

            if (isset($data['description'])) {
                $fields->description = $data['description'];
            }

            if (isset($data['description_short'])) {
                $fields->description_short = $data['description_short'];
            }

            if (isset($data['meta_title'])) {
                $fields->meta_title = $data['meta_title'];
            }

            if (isset($data['meta_description'])) {
                $fields->meta_description = $data['meta_description'];
            }

            if (isset($data['meta_keywords'])) {
                $fields->meta_keywords = $data['meta_keywords'];
            }

            //  $fields->position_in_category =null;
            //  $fields->manufacturer_name=null;
            //  $fields->quantity=null;

            unset($fields->position_in_category);
            unset($fields->manufacturer_name);
            unset($fields->quantity);

            try {
                $updatedXml = $this->webService->edit([
                    'resource' => 'products',
                    'id' => (int) $fields->id,
                    'putXml' => $xmlProduct->asXML(),
                ]);

                $fields = $updatedXml->product->children();

                echo 'Product updated with ID ' . $fields->id . PHP_EOL;
            } catch (Exception $ignored) {
                // do nothing... php will ignore and continue
                // but maybe use "ignored" as name to silence IDE warnings.  
                echo 'Product error with ID ' . $product_id . PHP_EOL;
                Log::info('Product error with ID ' . $product_id);
            }


        } else {
            echo 'Product not found with ID ' . $product_id . PHP_EOL;
            Log::info('Product not found with ID ' . $product_id);
        }
    }


    public function insertProductPrestashop($request)
    {

        $blankXml = $this->getProductSchemaPrestashop();


        try {
            // get the entity
            $fields = $blankXml->product->children();

            //dd($fields);

            // edit entity fields

            //----------------------//
            //required

            if (isset($request['price'])) {
                $fields->price = $request['price'];
            }

            //----------------------//

            if (isset($request['id_manufacturer'])) {
                $fields->id_manufacturer = $request['id_manufacturer'];
            }

            if (isset($request['id_supplier'])) {
                $fields->id_supplier = $request['id_supplier'];
            }

            $id_category_default = 2;
            if (isset($request['id_category_default'])) {

                $migrationCategory = MigrationCategory::where('category_old_id', $request['id_category_default'])->first();
                if ($migrationCategory) {
                    $id_category_default = $migrationCategory->category_new_id;
                    $fields->id_category_default = $id_category_default;
                }
            }

            if (isset($request['new'])) {
                $fields->new = $request['new'];
            }

            if (isset($request['cache_default_attribute'])) {
                $fields->cache_default_attribute = $request['cache_default_attribute'];
            }

            //----------------------//

            if (isset($request['id_default_image'])) {
                $fields->id_default_image = $request['id_default_image'];
            }

            if (isset($request['id_default_combination'])) {
                $fields->id_default_combination = $request['id_default_combination'];
            }

            $id_tax_rules_group = config('app.TAX_ID');

            /*
            if (isset($request['id_tax_rules_group'])) {
                $id_tax_rules_group = $request['id_tax_rules_group'];
            }
            */
            $fields->id_tax_rules_group = $id_tax_rules_group;


            if (isset($request['position_in_category'])) {
                $fields->position_in_category = $request['position_in_category'];
            }

            //----------------------//

            if (isset($request['type'])) {
                $fields->type = $request['type'];
            }

            if (isset($request['id_shop_default'])) {
                $fields->id_shop_default = $request['id_shop_default'];
            }

            if (isset($request['reference'])) {
                $fields->reference = $request['reference'];
            }

            if (isset($request['supplier_reference'])) {
                $fields->supplier_reference = $request['supplier_reference'];
            }

            //----------------------//

            if (isset($request['location'])) {
                $fields->location = $request['location'];
            }

            if (isset($request['width'])) {
                $fields->width = $request['width'];
            }

            if (isset($request['height'])) {
                $fields->height = $request['height'];
            }

            if (isset($request['depth'])) {
                $fields->depth = $request['depth'];
            }

            if (isset($request['weight'])) {
                $fields->weight = $request['weight'];
            }

            //----------------------//

            if (isset($request['quantity_discount'])) {
                $fields->quantity_discount = $request['quantity_discount'];
            }

            if (isset($request['ean13'])) {
                $fields->ean13 = $request['ean13'];
            }

            if (isset($request['isbn'])) {
                $fields->isbn = $request['isbn'];
            }

            if (isset($request['upc'])) {
                $fields->upc = $request['upc'];
            }

            if (isset($request['mpn'])) {
                $fields->mpn = $request['mpn'];
            }

            //----------------------//

            if (isset($request['cache_is_pack'])) {
                $fields->cache_is_pack = $request['cache_is_pack'];
            }

            if (isset($request['cache_has_attachments'])) {
                $fields->cache_has_attachments = $request['cache_has_attachments'];
            }

            if (isset($request['is_virtual'])) {
                $fields->is_virtual = $request['is_virtual'];
            }

            if (isset($request['state'])) {
                $fields->state = $request['state'];
            }

            if (isset($request['additional_delivery_times'])) {
                $fields->additional_delivery_times = $request['additional_delivery_times'];
            }

            //----------------------//

            if (isset($request['delivery_in_stock'])) {
                $fields->delivery_in_stock->language = $request['delivery_in_stock'];
            }

            if (isset($request['delivery_out_stock'])) {
                $fields->delivery_out_stock->language = $request['delivery_out_stock'];
            }

            if (isset($request['on_sale'])) {
                $fields->on_sale = $request['on_sale'];
            }

            if (isset($request['online_only'])) {
                $fields->online_only = $request['online_only'];
            }

            if (isset($request['ecotax'])) {
                $fields->ecotax = $request['ecotax'];
            }

            //----------------------//

            if (isset($request['minimal_quantity'])) {
                $fields->minimal_quantity = $request['minimal_quantity'];
            }

            if (isset($request['low_stock_threshold'])) {
                $fields->low_stock_threshold = $request['low_stock_threshold'];
            }

            if (isset($request['low_stock_alert'])) {
                $fields->low_stock_alert = $request['low_stock_alert'];
            }


            if (isset($request['wholesale_price'])) {
                $fields->wholesale_price = $request['wholesale_price'];
            }

            //----------------------//

            if (isset($request['unity'])) {
                $fields->unity = $request['unity'];
            }

            if (isset($request['unit_price_ratio'])) {
                $fields->unit_price_ratio = $request['unit_price_ratio'];
            }

            if (isset($request['additional_shipping_cost'])) {
                $fields->additional_shipping_cost = $request['additional_shipping_cost'];
            }


            if (isset($request['customizable'])) {
                $fields->customizable = $request['customizable'];
            }

            if (isset($request['text_fields'])) {
                $fields->text_fields = $request['text_fields'];
            }

            //----------------------//

            if (isset($request['uploadable_files'])) {
                $fields->uploadable_files = $request['uploadable_files'];
            }

            if (isset($request['active'])) {
                $fields->active = $request['active'];
            }

            if (isset($request['redirect_type'])) {
                $fields->redirect_type = $request['redirect_type'];
            }

            if (isset($request['id_type_redirected'])) {
                $fields->id_type_redirected = $request['id_type_redirected'];
            }

            if (isset($request['available_for_order'])) {
                $fields->available_for_order = $request['available_for_order'];
            }

            //----------------------//

            if (isset($request['available_date'])) {
                $fields->available_date = $request['available_date'];
            }

            if (isset($request['show_condition'])) {
                $fields->show_condition = $request['show_condition'];
            }

            if (isset($request['condition'])) {
                $fields->condition = $request['condition'];
            }

            if (isset($request['show_price'])) {
                $fields->show_price = $request['show_price'];
            }

            if (isset($request['indexed'])) {
                $fields->indexed = $request['indexed'];
            }

            //----------------------//

            if (isset($request['visibility'])) {
                $fields->visibility = $request['visibility'];
            }

            if (isset($request['advanced_stock_management'])) {
                $fields->advanced_stock_management = $request['advanced_stock_management'];
            }

            if (isset($request['date_add'])) {
                $fields->date_add = $request['date_add'];
            }

            if (isset($request['date_upd'])) {
                $fields->date_upd = $request['date_upd'];
            }

            if (isset($request['pack_stock_type'])) {
                $fields->pack_stock_type = $request['pack_stock_type'];
            }

            //----------------------//           


            if (isset($request['meta_description'])) {
                $fields->meta_description->language = $request['meta_description'];
            }
            if (isset($request['meta_keywords'])) {
                $fields->meta_keywords->language = $request['meta_keywords'];
            }

            if (isset($request['meta_title'])) {
                $fields->meta_title->language = $request['meta_title'];
            }

            if (isset($request['link_rewrite'])) {
                $fields->link_rewrite->language = $request['link_rewrite'];
            }

            if (isset($request['name'])) {
                $fields->name->language = $request['name'];
            }

            //----------------------//   

            if (isset($request['description'])) {
                $fields->description->language = $request['description'];
            }

            if (isset($request['description_short'])) {
                $fields->description_short->language = $request['description_short'];
            }

            if (isset($request['available_now'])) {
                $fields->available_now->language = $request['available_now'];
            }

            if (isset($request['available_later'])) {
                $fields->available_later->language = $request['available_later'];
            }

            /*
            if (isset($request['associations'])) {
                $fields->associations = $request['associations'];
            }
            */

            $fields->associations->categories->category->id = $id_category_default;
            //$fields->associations->categories->category = $id_category_default ;
            //$fields->associations->categories = $id_category_default ;

            //-----------------//   
            //NO WRITABLE

            if (isset($request['manufacturer_name '])) {
                $fields->manufacturer_name = $request['manufacturer_name '];
            }

            /*
            if (isset($request['quantity'])) {
                $fields->quantity = $request['quantity'];
            }
*/
            //-----------------//   

            //var_dump($blankXml->asXML());

            // send entity to webservice
            $createdXml = $this->webService->add([
                'resource' => 'products',
                'postXml' => $blankXml->asXML(),
            ]);
            $newFields = $createdXml->product->children();

            Log::info('-- insertProductPrestashopData -- created with ID ' . $newFields->id . ' | category: ' . $id_category_default);

            //set id

            $newProduct = MigrationProduct::where('product_old_id', $request['id'])->first();

            if ($newProduct) {
                $newProduct->product_new_id = $newFields->id;
                $newProduct->save();
            } else {
                MigrationProduct::create([
                    'product_old_id' => $request['id'],
                    'product_new_id' => $newFields->id,
                ]);
            }
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info(' insertProductPrestashopData -- Other error: ' .            $ex->getMessage());
        }
    }


    public function insertProductPrestashopTest(Request $request)
    {

        $request['id'] = 50;
        $request['price'] = 1000;


        $request['active'] = true;
        $request['name'] = 'Product1';
        $request['description'] = 'Description product1-';
        $request['description_short'] = 'ShortDescription product1-';
        $request['link_rewrite'] =  'product1';

        $request['id_category_default'] =  600;
        $request['position_in_category'] =  1;
        $request['date_add'] =  '2023-08-31';
        $request['date_upd'] =  '2023-08-31';

        $request['meta_title'] =  'Prueba Product1 -title';
        $request['meta_description'] =  'Prueba Product32-description';
        $request['meta_keywords'] =  'Prueba Product4 -keywords';

        $request['quantity'] = 1;

        return $this->insertProductPrestashop($request);
    }


    public function migrateProducts()
    {

        Log::info('=================================================');
        Log::info('ProductController - migrateProducts');
        Log::info('migrateProducts - start');
        $start = Carbon::now();


        $saltear = config('app.SALTEAR');
        $tomar = config('app.TOMAR');
        $prestahopUrl = config('app.PRESTASHOP_URL');

        $webServices = $prestahopUrl . 'products';

        $products = $this->getList($saltear, $tomar);

        $count = $products->count();

        Log::info('$count ' . $count);

        $process = 0;

        $request = [];

        $productsStore = $this->getProductsPrestashop();


        while ($products->count()) {
            foreach ($products as $product) {

                $productLang = $product->products_lang->where('id_lang', 7)->first();

                $migrationProduct = MigrationProduct::where('product_old_id',  $product->id_product)->first();

                if (!$migrationProduct  || !array_search($migrationProduct->product_new_id, $productsStore)) {
                    //not migrationProduct -- or -- migrationProduct and not array

                    $requestBase = $request;


                    $requestBase['id'] = $product->id_product;

                    $requestBase['id_product '] = $product->id_product;
                    $requestBase['id_supplier'] = $product->id_supplier; //id_supplier
                    $requestBase['id_manufacturer'] = $product->id_manufacturer; //id_manufacturer
                    $requestBase['id_category_default'] = $product->id_category_default; //id_category_default
                    $requestBase['id_shop_default'] = $product->id_shop_default; //id_shop_default

                    $requestBase['id_tax_rules_group'] = $product->id_tax_rules_group; //id_tax_rules_group
                    $requestBase['on_sale'] = $product->on_sale; //on_sale
                    $requestBase['online_only'] = $product->online_only; //online_only
                    $requestBase['ean13'] = $product->ean13; //ean13
                    $requestBase['upc'] = $product->upc; //upc

                    $requestBase['ecotax'] = $product->ecotax; //ecotax
                    $requestBase['quantity'] = $product->quantity; //quantity
                    $requestBase['minimal_quantity'] = $product->minimal_quantity; //minimal_quantity
                    $requestBase['price'] = $product->price; //price
                    $requestBase['wholesale_price'] = $product->wholesale_price; //wholesale_price

                    $requestBase['unity'] = $product->unity; //unity
                    $requestBase['unit_price_ratio'] = $product->unit_price_ratio; //unit_price_ratio
                    $requestBase['additional_shipping_cost'] = $product->additional_shipping_cost; //additional_shipping_cost
                    $requestBase['reference'] = $product->reference; //reference
                    $requestBase['supplier_reference'] = $product->supplier_reference; //supplier_reference

                    $requestBase['location'] = $product->location; //location
                    $requestBase['width'] = $product->width; //width
                    $requestBase['height'] = $product->height; //height
                    $requestBase['depth'] = $product->depth; //depth
                    $requestBase['weight'] = $product->weight; //weight

                    $requestBase['out_of_stock'] = $product->out_of_stock;
                    $requestBase['quantity_discount'] = $product->quantity_discount; //quantity_discount
                    $requestBase['customizable'] = $product->customizable; //customizable
                    $requestBase['uploadable_files'] = $product->uploadable_files; //uploadable_files
                    $requestBase['text_fields'] = $product->text_fields; //text_fields

                    $requestBase['active'] = $product->active; //active
                    $requestBase['redirect_type'] = $product->redirect_type; //redirect_type
                    $requestBase['id_product_redirected'] = $product->id_product_redirected;
                    $requestBase['available_for_order'] = $product->available_for_order; //available_for_order
                    $requestBase['available_date'] = $product->available_date; //available_date

                    $requestBase['condition'] = $product->condition; //condition
                    $requestBase['show_price'] = $product->show_price; //show_price
                    $requestBase['indexed'] = $product->indexed; //indexed
                    $requestBase['visibility'] = $product->visibility; //visibility
                    $requestBase['cache_is_pack'] = $product->cache_is_pack; //cache_is_pack

                    $requestBase['cache_has_attachments'] = $product->cache_has_attachments; //cache_has_attachments
                    $requestBase['is_virtual'] = $product->is_virtual; //is_virtual
                    $requestBase['cache_default_attribute'] = $product->cache_default_attribute;    //cache_default_attribute           
                    $requestBase['date_add'] =  $product->date_add; //date_add
                    $requestBase['date_upd'] =  $product->date_upd; //date_upd

                    $requestBase['advanced_stock_management'] = $product->advanced_stock_management; //advanced_stock_management


                    if ($productLang) {
                        $requestBase['name'] = ($productLang->name ? $productLang->name : '--'); //name
                        $requestBase['description'] = ($productLang->description ? $productLang->description : '--'); //description
                        $requestBase['link_rewrite'] =  ($productLang->link_rewrite ? $productLang->link_rewrite : '--'); //link_rewrite

                        $requestBase['meta_title'] =  ($productLang->meta_title ? $productLang->meta_title : '--'); //meta_title
                        $requestBase['meta_description'] =  ($productLang->meta_description ? $productLang->meta_description : '--'); //meta_description
                        $requestBase['meta_keywords'] =  ($productLang->meta_keywords ? $productLang->meta_keywords : '--'); //meta_keywords
                    }

                    $requestBase['state'] = 1; //state

                    //$requestBase['new'] = ; //new
                    //$requestBase['id_default_image'] = ; //id_default_image
                    //$requestBase['id_default_combination'] = ; //id_default_combination
                    //$requestBase['position_in_category'] = 1; //position_in_category
                    //$requestBase['manufacturer_name'] = ; //manufacturer_name
                    //$requestBase['type'] = ; //type
                    //$requestBase['isbn'] = ; //isbn
                    //$requestBase['mpn'] = ; //mpn

                    //$requestBase['additional_delivery_times'] = ; //additional_delivery_times
                    //$requestBase['delivery_in_stock'] = ; //delivery_in_stock
                    //$requestBase['delivery_out_stock'] = ; //delivery_out_stock
                    //$requestBase['low_stock_threshold'] = ; //low_stock_threshold
                    //$requestBase['low_stock_alert'] = ; //low_stock_alert
                    //$requestBase['id_type_redirected'] = ; //id_type_redirected
                    //$requestBase['show_condition'] = ; //show_condition
                    //$requestBase['pack_stock_type'] = ; //pack_stock_type
                    //$requestBase['description_short'] = ; //description_short
                    //$requestBase['available_now'] = ; //available_now
                    //$requestBase['available_later'] = ; //available_later	
                    //$requestBase['associations'] = ; //associations	



                    //Log::info($request);

                    Log::info('id: ' . $requestBase['id'] .   ' | name: ' . (isset($requestBase['name']) ? $requestBase['name'] : '--'));



                    $this->insertProductPrestashop($requestBase);

                    $process++;
                } else {
                    Log::info('product exist: ' . $product->id_product . ' | ' . $productLang->name);
                }
            }

            $saltear = $saltear + $tomar;

            $products = $this->getList($saltear, $tomar);
        }

        Log::info('$process ' . $process);
        $end = Carbon::now();
        Log::info('migrateProducts - end - ' . $start->diffInSeconds($end) . ' seconds');
        Log::info('=================================================');
    }


    public function updateMigrateProducts()
    {

        Log::info('=================================================');
        Log::info('ProductController - updateMigrateProducts');
        Log::info('updateMigrateProducts - start');
        $start = Carbon::now();


        $saltear = config('app.SALTEAR');
        $tomar = config('app.TOMAR');
        $prestahopUrl = config('app.PRESTASHOP_URL');

        $webServices = $prestahopUrl . 'products';

        $products = $this->getList($saltear, $tomar);

        $count = $products->count();

        Log::info('$count ' . $count);

        $process = 0;

        $request = [];
        $EXCLUDE_PRODUCT_ID = explode(",", config('app.EXCLUDE_PRODUCT_ID'));

        $productsStore = $this->getProductsPrestashop();


        while ($products->count()) {
            foreach ($products as $product) {

                $productLang = $product->products_lang->where('id_lang', 4)->first();

                $requestBase = $request;

                /*
                $requestBase['id'] = $product->id_product;
                $requestBase['id_product '] = $product->id_product;
                */

                $migrationProduct = MigrationProduct::where('product_old_id',  $product->id_product)->first();

                if ($migrationProduct) {

                    if (!in_array($migrationProduct->product_new_id, $EXCLUDE_PRODUCT_ID)) {
                        $requestBase['id'] = $migrationProduct->product_new_id;
                        $requestBase['id_product '] = $migrationProduct->product_new_id;

                        /*
    $requestBase['id_supplier'] = $product->id_supplier; //id_supplier
    $requestBase['id_manufacturer'] = $product->id_manufacturer; //id_manufacturer
    $requestBase['id_category_default'] = $product->id_category_default; //id_category_default
    $requestBase['id_shop_default'] = $product->id_shop_default; //id_shop_default

    $requestBase['id_tax_rules_group'] = $product->id_tax_rules_group; //id_tax_rules_group
    $requestBase['on_sale'] = $product->on_sale; //on_sale
    $requestBase['online_only'] = $product->online_only; //online_only
    $requestBase['ean13'] = $product->ean13; //ean13
    $requestBase['upc'] = $product->upc; //upc

    $requestBase['ecotax'] = $product->ecotax; //ecotax
    $requestBase['quantity'] = $product->quantity; //quantity
    $requestBase['minimal_quantity'] = $product->minimal_quantity; //minimal_quantity
    $requestBase['price'] = $product->price; //price
    $requestBase['wholesale_price'] = $product->wholesale_price; //wholesale_price

    $requestBase['unity'] = $product->unity; //unity
    $requestBase['unit_price_ratio'] = $product->unit_price_ratio; //unit_price_ratio
    $requestBase['additional_shipping_cost'] = $product->additional_shipping_cost; //additional_shipping_cost
    $requestBase['reference'] = $product->reference; //reference
    $requestBase['supplier_reference'] = $product->supplier_reference; //supplier_reference

    $requestBase['location'] = $product->location; //location
    $requestBase['width'] = $product->width; //width
    $requestBase['height'] = $product->height; //height
    $requestBase['depth'] = $product->depth; //depth
    $requestBase['weight'] = $product->weight; //weight

    $requestBase['out_of_stock'] = $product->out_of_stock;
    $requestBase['quantity_discount'] = $product->quantity_discount; //quantity_discount
    $requestBase['customizable'] = $product->customizable; //customizable
    $requestBase['uploadable_files'] = $product->uploadable_files; //uploadable_files
    $requestBase['text_fields'] = $product->text_fields; //text_fields

    $requestBase['active'] = $product->active; //active
    $requestBase['redirect_type'] = $product->redirect_type; //redirect_type
    $requestBase['id_product_redirected'] = $product->id_product_redirected;
    $requestBase['available_for_order'] = $product->available_for_order; //available_for_order
    $requestBase['available_date'] = $product->available_date; //available_date

    $requestBase['condition'] = $product->condition; //condition
    $requestBase['show_price'] = $product->show_price; //show_price
    $requestBase['indexed'] = $product->indexed; //indexed
    $requestBase['visibility'] = $product->visibility; //visibility
    $requestBase['cache_is_pack'] = $product->cache_is_pack; //cache_is_pack

    $requestBase['cache_has_attachments'] = $product->cache_has_attachments; //cache_has_attachments
    $requestBase['is_virtual'] = $product->is_virtual; //is_virtual
    $requestBase['cache_default_attribute'] = $product->cache_default_attribute;    //cache_default_attribute
    $requestBase['date_add'] =  $product->date_add; //date_add
    $requestBase['date_upd'] =  $product->date_upd; //date_upd

    $requestBase['advanced_stock_management'] = $product->advanced_stock_management; //advanced_stock_management
    */

                        if ($productLang) {
                            $requestBase['name'] =   '--';
                            if ($productLang->name) {
                                $requestBase['name'] =  $this->codificarAcentos($productLang->name); //name
                                $requestBase['name'] = trim($requestBase['name']);
                            }


                            $requestBase['description'] =   '--'; //description
                            if ($productLang->description) {
                                // $requestBase['description'] = $this->xss_clean($productLang->description); //description
                                //$requestBase['description'] = utf8_encode($productLang->description); //description
                                // $requestBase['description'] = simplexml_load_string(utf8_encode($productLang->description)); //description
                                //$requestBase['description'] = mb_convert_encoding($productLang->description, 'UTF-8'); //description
                                //$requestBase['description'] = $this->codificarAcentos($productLang->description); //description
                                $requestBase['description'] = $productLang->description; //description
                            }
                            $requestBase['description_short'] = '--'; //description
                            if ($productLang->description_short) {
                                //$requestBase['description_short'] = $this->xss_clean($productLang->description_short); //description
                                // $requestBase['description_short'] = utf8_encode($productLang->description_short); //description
                                //$requestBase['description_short'] = simplexml_load_string(utf8_encode($productLang->description_short)); //description
                                //$requestBase['description_short'] = mb_convert_encoding($productLang->description_short, 'UTF-8'); //description
                                //$requestBase['description_short'] = $this->codificarAcentos($productLang->description_short); //description
                                $requestBase['description_short'] = $productLang->description_short; //description

                            }
                            $requestBase['link_rewrite'] =  ($productLang->link_rewrite ? $productLang->link_rewrite : '--'); //link_rewrite

                            if ($productLang->meta_title && $productLang->meta_title != '--') {
                                $requestBase['meta_title'] =  $productLang->meta_title; //meta_title
                            } else {
                                $requestBase['meta_title'] =  ''; //meta_title
                            }

                            if ($productLang->meta_description && $productLang->meta_description != '--') {
                                $requestBase['meta_description'] =  $productLang->meta_description; //meta_description
                            } else {
                                $requestBase['meta_description'] =  ''; //meta_title
                            }

                            if ($productLang->meta_keywords && $productLang->meta_keywords != '--') {
                                $requestBase['meta_keywords'] =   $productLang->meta_keywords; //meta_keywords
                            } else {
                                $requestBase['meta_keywords'] =  ''; //meta_title
                            }

                            $requestBase = mb_convert_encoding($requestBase, 'UTF-8');
                        }


                        //$requestBase['state'] = 1; //state

                        Log::info('id: ' . $requestBase['id'] . ' | old id: ' . $product->id_product .  ' | name: ' . (isset($requestBase['name']) ? $requestBase['name'] : '--'));

                        $this->updateProductPrestashop($migrationProduct->product_new_id, $requestBase);

                        $process++;
                    }
                }
            }

            $saltear = $saltear + $tomar;

            $products = $this->getList($saltear, $tomar);
        }

        Log::info('$process ' . $process);
        $end = Carbon::now();
        Log::info('migrateProducts - end - ' . $start->diffInSeconds($end) . ' seconds');
        Log::info('=================================================');
    }


    public function deleteProducts()
    {
        Log::info('=================================================');
        Log::info('deleteProducts - start');
        $start = Carbon::now();

        try {

            // call to retrieve all products
            $xml = $this->webService->get(['resource' => 'products']);
        } catch (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            Log::info('deleteProducts - Other error: ' . $ex->getMessage());
        }

        $resources = $xml->products->children();
        foreach ($resources as $resource) {
            $attributes = $resource->attributes();
            $resourceId = $attributes['id'];
            // From there you could, for example, use th resource ID to call the webservice to get its details


            try {

                //$id = 2;
                $this->webService->delete([
                    'resource' => 'products',
                    'id' =>  $resourceId, // Here we use hard coded value but of course you could get this ID from a request parameter or anywhere else
                ]);
                Log::info('Product with ID ' . $resourceId . ' was successfully deleted ');
            } catch (PrestaShopWebserviceException $e) {
                Log::info('deleteProducts - Error:' . $e->getMessage());
            }
        }

        $end = Carbon::now();
        Log::info('deleteProducts - end - ' . $start->diffInSeconds($end) . ' seconds');
        Log::info('=================================================');
    }

    public function migrateProductImages()
    {
        Log::info('=================================================');
        Log::info('migrateProductImages - start');
        $start = Carbon::now();

        $saltear = config('app.SALTEAR');
        $tomar = config('app.TOMAR');
        $products = $this->getList($saltear, $tomar);

        $process = 0;
        $key  = config('app.PRESTASHOP_API_TOKEN');

        while ($products->count()) {
            foreach ($products as $product) {
                $migrationProduct = MigrationProduct::where('product_old_id', $product->id_product)->orderBy('product_new_id', 'DESC')->first();

                if ($migrationProduct) {
                    foreach ($product->images as $image) {
                        $idImage = $image->id_image;


                        $urlImage = config('app.PRESTASHOP_URL_API_IMAGES') . $migrationProduct->product_new_id . '/';

                        Log::info($urlImage);

                        $subdirImage = implode('/', str_split($idImage));
                        $file = $idImage . '-thickbox_default.jpg';

                        //Here you set the path to the image you need to upload
                        //$image_path = '/path/to/the/image.jpg';
                        $image_path = 'http://semty.mx/img/p/' . $subdirImage . '/' . $file;
                        Log::info($image_path);

                        $image_mime = 'image/jpg';

                        $args['image'] = new CURLFile($image_path, $image_mime);

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_HEADER, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
                        curl_setopt($ch, CURLOPT_URL, $urlImage);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_USERPWD, $key . ':');
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
                        $result = curl_exec($ch);
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);

                        if (200 == $httpCode) {
                            Log::info('Product image was successfully created. [product_old_id: ' . $migrationProduct->product_old_id . '] [product_new_id: ' . $migrationProduct->product_new_id . '] - ' . $image_path);
                        }
                    }
                }
            }
            $saltear = $saltear + $tomar;

            $products = $this->getList($saltear, $tomar);
        }

        $end = Carbon::now();
        Log::info('migrateProductImages - end - ' . $start->diffInSeconds($end) . ' seconds');
        Log::info('=================================================');
    }





    /*
 * XSS filter 
 *
 * This was built from numerous sources
 * (thanks all, sorry I didn't track to credit you)
 * 
 * It was tested against *most* exploits here: http://ha.ckers.org/xss.html
 * WARNING: Some weren't tested!!!
 * Those include the Actionscript and SSI samples, or any newer than Jan 2011
 *
 *
 * TO-DO: compare to SymphonyCMS filter:
 * https://github.com/symphonycms/xssfilter/blob/master/extension.driver.php
 * (Symphony's is probably faster than my hack)
 */

    public function xss_clean($data)
    {
        // Fix &entity\n;
        $data = str_replace(array('&', '<', '>'), array('&amp;', '&lt;', '&gt;'), $data);
        $data = preg_replace("/(*\w+)[\x00-\x20]+;/u", '$1;', $data);
        $data = preg_replace('/(*[0-9A-F]+);*/iu', '$1;', $data);
        $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

        // Remove any attribute starting with "on" or xmlns
        $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

        // Remove javascript: and vbscript: protocols
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

        // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

        // Remove namespaced elements (we do not need them)
        $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

        do {
            // Remove really unwanted tags
            $old_data = $data;
            $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
        } while ($old_data !== $data);

        // we are done...
        return $data;
    }

    public function fix_latin1_mangled_with_utf8_maybe_hopefully_most_of_the_time($str)
    {
        return preg_replace_callback('#[\\xA1-\\xFF](?![\\x80-\\xBF]{2,})#', 'utf8_encode_callback', $str);
    }

    public function utf8_encode_callback($m)
    {
        return utf8_encode($m[0]);
    }

    public function codificarAcentos($texto)
    {
        $texto = str_replace("Á", "A", $texto);
        $texto = str_replace("É", "E", $texto);
        $texto = str_replace("Í", "I", $texto);
        $texto = str_replace("Ó", "O", $texto);
        $texto = str_replace("Ú", "U", $texto);

        $texto = str_replace("á", "a", $texto);
        $texto = str_replace("é", "e", $texto);
        $texto = str_replace("í", "i", $texto);
        $texto = str_replace("ó", "o", $texto);
        $texto = str_replace("ú", "u", $texto);
        $texto = str_replace("ñ", "n", $texto);

        /*
        á -> &aacute;
        é -> &eacute;
        í -> &iacute;
        ó -> &oacute;
        ú -> &uacute;
        ñ -> &ntilde;
        */

        return $texto;
    }
}
