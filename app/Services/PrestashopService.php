<?php

namespace App\Services;

use PrestaShopWebservice;

class PrestashopService
{
    public function createTokenAccess()
    {
        // creating webservice access
        $webService = new PrestaShopWebservice(config('app.PRESTASHOP_URL'), config('app.PRESTASHOP_API_TOKEN'), false);
        return $webService;
    }
}