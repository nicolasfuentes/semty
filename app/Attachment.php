<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    /*
`id_attachment` int(10) UNSIGNED NOT NULL,
  `file` varchar(40) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `mime` varchar(128) NOT NULL
    */

    protected $table = 'ps_attachment';

    protected $primaryKey = 'id_attachment';

    protected $fillable = [
        'id_attachment',
        'file',
        'file_name',
        'mime'
    ];

      // RELATIONS
  public function attachments_lang()
  {
    return $this->hasMany('App\AttachmentLang', 'id_attachment', 'id_attachment');
  }
}
