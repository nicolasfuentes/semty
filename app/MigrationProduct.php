<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MigrationProduct extends Model
{
    protected $table = 'ps_migration_products';

    protected $fillable = [
        'product_old_id',
        'product_new_id'
    ];

    public $timestamps = false;
}
