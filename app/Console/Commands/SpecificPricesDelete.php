<?php

namespace App\Console\Commands;

use App\Services\SpecificPricesService;
use Illuminate\Console\Command;

class SpecificPricesDelete extends Command
{
    protected $specificPricesService;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'specific-prices:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'specific prices delete all';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SpecificPricesService $specificPricesService)
    {
        $this->specificPricesService = $specificPricesService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->specificPricesService->deleteSpecificPrices();
    }
}
