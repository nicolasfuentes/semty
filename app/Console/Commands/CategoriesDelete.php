<?php

namespace App\Console\Commands;

use App\Services\CategoriesService;
use Illuminate\Console\Command;

class CategoriesDelete extends Command
{
    protected $categoriesService;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'categories delete all';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CategoriesService $categoriesService)
    {
        $this->categoriesService = $categoriesService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->categoriesService->deleteCategories();
    }
}
