<?php

namespace App\Console\Commands;

use App\Services\CategoriesService;
use Illuminate\Console\Command;

class CategoriesMigrate extends Command
{
    protected  $categoriesService;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'categories migrate all';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CategoriesService $categoriesService)
    {
        $this->categoriesService = $categoriesService;


        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->categoriesService->migrateCategories();

        return 0;
    }
}
