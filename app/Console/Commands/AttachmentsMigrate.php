<?php

namespace App\Console\Commands;

use App\Services\AttachmentsService;
use Illuminate\Console\Command;

class AttachmentsMigrate extends Command
{
    protected  $attachmentsService;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attachments:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'attachments migrate all';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AttachmentsService $attachmentsService)
    {
        $this->attachmentsService = $attachmentsService;


        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->attachmentsService->migrateAttachments();

        return 0;
    }
}
