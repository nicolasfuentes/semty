<?php

namespace App\Console\Commands;

use App\Services\ProductsService;
use Illuminate\Console\Command;

class ProductsDelete extends Command
{
    protected $productsService;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'products delete all';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ProductsService $productsService)
    {
        $this->productsService = $productsService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->productsService->deleteProducts();
    }
}
