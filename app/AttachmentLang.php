<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttachmentLang extends Model
{
    /*
  `id_attachment` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` text DEFAULT NULL
    */

    protected $table = 'ps_attachment_lang';

    protected $primaryKey = 'id_attachment';
  
    protected $fillable = [
        'id_attachment',
        'id_lang',
        'name',
        'description'
    ];

}
