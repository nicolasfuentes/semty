<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  /*
    `id_category` int(10) UNSIGNED NOT NULL,
  `id_parent` int(10) UNSIGNED NOT NULL,
  `id_shop_default` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `level_depth` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `nleft` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `nright` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `is_root_category` tinyint(1) NOT NULL DEFAULT 0
  */

  protected $table = 'ps_category';

  protected $primaryKey = 'id_category';

  protected $fillable = [
    'id_category',
    'id_parent',
    'id_shop_default',
    'level_depth',
    'nleft',
    'nright',
    'active',
    'date_add',
    'date_upd',
    'position',
    'is_root_category'
  ];

  // RELATIONS
  public function categories_lang()
  {
    return $this->hasMany('App\CategoryLang', 'id_category', 'id_category');
  }
}
