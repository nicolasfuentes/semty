<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryLang extends Model
{
    /*
    `id_category` int(10) UNSIGNED NOT NULL,
    `id_shop` int(11) UNSIGNED NOT NULL DEFAULT 1,
    `id_lang` int(10) UNSIGNED NOT NULL,
    `name` varchar(128) NOT NULL,
    `description` text DEFAULT NULL,
    `link_rewrite` varchar(128) NOT NULL,
    `meta_title` varchar(128) DEFAULT NULL,
    `meta_keywords` varchar(255) DEFAULT NULL,
    `meta_description` varchar(255) DEFAULT NULL
    */

    protected $table = 'ps_category_lang';

    //protected $primaryKey = ['id_category', 'id_shop', 'id_lang'];
    //public $incrementing = false;

    protected $fillable = [
        'id_category',
        'id_shop',
        'id_lang',
        'name',
        'description',
        'link_rewrite',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];
}
