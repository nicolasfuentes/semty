<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SpecificPriceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('index', function () {
    return view('welcome');
});

*/

//*********************************** */
Route::get('categories', [CategoryController::class, 'getCategories']);

Route::get('categoriesPrestashop', [CategoryController::class, 'getCategoriesPrestashop']);
Route::get('categoryPrestashop/{id_category?}', [CategoryController::class, 'getCategoryPrestashop']);
Route::get('categoryPrestashopSchema/{print?}', [CategoryController::class, 'getCategorySchemaPrestashop']);

Route::get('getCategoryPrestashopTest', [CategoryController::class, 'getCategoryPrestashopTest']);


Route::get('insertCategoryPrestashop', [CategoryController::class, 'insertCategoryPrestashop']);
Route::get('insertCategoryPrestashopTest', [CategoryController::class, 'insertCategoryPrestashopTest']);

Route::get('migrateCategories', [CategoryController::class, 'migrateCategories']);

Route::get('deleteCategories', [CategoryController::class, 'deleteCategories']);


//*********************************** */

Route::get('products', [ProductController::class, 'getProducts']);

Route::get('productsPrestashop', [ProductController::class, 'getProductsPrestashop']);
Route::get('productPrestashop/{product_id}', [ProductController::class, 'getProductPrestashop']);
Route::get('productPrestashopSchema/{print?}', [ProductController::class, 'getProductSchemaPrestashop']);

Route::get('insertProductPrestashop', [ProductController::class, 'insertProductPrestashop']);
Route::get('insertProductPrestashopTest', [ProductController::class, 'insertProductPrestashopTest']);

Route::get('updateProductPrestashop/{product_id}/{data}', [ProductController::class, 'updateProductPrestashop']);


Route::get('migrateProducts', [ProductController::class, 'migrateProducts']);

Route::get('deleteProducts', [ProductController::class, 'deleteProducts']);

Route::get('migrateProductImages', [ProductController::class, 'migrateProductImages']);


//*********************************** */
Route::get('specificPrices', [SpecificPriceController::class, 'getSpecificPrices']);

Route::get('specificPricesPrestashop', [SpecificPriceController::class, 'getSpecificPricesPrestashop']);
Route::get('specificPricePrestashop', [SpecificPriceController::class, 'getSpecificPricePrestashop']);
Route::get('specificPricePrestashopSchema/{print?}', [SpecificPriceController::class, 'getSpecificPriceSchemaPrestashop']);

Route::get('insertSpecificPricePrestashop', [SpecificPriceController::class, 'insertSpecificPricePrestashop']);

Route::get('migrateSpecificPrices', [SpecificPriceController::class, 'migrateSpecificPrices']);

Route::get('deleteSpecificPrices', [SpecificPriceController::class, 'deleteSpecificPrices']);


Route::get('images', [ImageController::class, 'getImages']);
